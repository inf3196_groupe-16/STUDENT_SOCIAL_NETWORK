"""ssn URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from ssn_app.views import *
from django.conf.urls.static import static
from ssn import settings

urlpatterns = [
    path('ssn', index, name='index'),
    path('ssn/login', login, name='login'),
    path('ssn/edit_profil', edit_profil, name='edit_profil'),
    path('ssn/edit_profil_ens', edit_profil_ens, name='edit_profil_ens'),
    path('ssn/dashbord', dashbord, name='dashbord'),
    path('ssn/dashbord_ad', dashbord_ad, name='dashbord_ad'),
    path('ssn/gestion_ens', gestion_ens, name='gestion_ens'),
    path('ssn/dashbord_ens', dashbord_ens, name='dashbord_ens'),
    path('ssn/profil', profil, name='profil'),
    path('ssn/profil_public', profil_public, name='profil_public'),
    path('ssn/profil_ens', profil_ens, name='profil_ens'),
    path('ssn/cours_etd', cours_etd, name='cours_etd'),
    path('ssn/cours_ens', cours_ens, name='cours_ens'),
    path('ssn/message', message, name='message'),
    path('ssn/message_ens', dashbord, name='message_ens'),
    path('ssn/notes', notes, name='notes'),
    path('ssn/liste_etd', list_etd, name='list_etd'),
    path('ssn/deconnexion', deconnexion, name='deconnexion'),
    path('ssn/like_post', like_post, name='like_post'),
    path('ssn/like_fil_actu', like_fil_actu, name='like_fil_actu'),
    path('admin/', admin.site.urls),
] + static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
