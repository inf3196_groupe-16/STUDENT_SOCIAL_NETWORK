<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="STUDENT SOCIAL NETWORK">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
<title> <?php 
        echo isset($title)? $title.' - STUDENT SOCIAL NETWORK':
        'STUDENT SOCIAL NETWORK' ?></title>
  <!-- Favicons -->
  <link rel="icon" href="../assets/fandy electronique logo.jpg">


  <link href="../assets/CSS1/bootstrap.css" rel="stylesheet">
        <link href="../librairies/parsley/parsley.css" rel="stylesheet">
        <link rel="stylesheet" href="../assets/CSS1/main.css">

         <!-- Bootstrap core JS-->
        <!-- Core theme JS-->
        <script src="../assets/JS/bootstrap.bundle.js"></script>
        <script src="../assets/JS/jquery.js"></script>
        <script src="../assets/JS/scripts.js"></script>
        <script src="../librairies/parsley/parsley.js"></script>
        <script src="../librairies/parsley/langue/fr.js"></script>
  

  <!-- Bootstrap core CSS1 -->
  <!--external css-->
  <link href="../assets/lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="../assets/lib/gritter/css/jquery.gritter.css" />

  <!-- Custom styles for this template -->
  <link href="../assets/css/style.css" rel="stylesheet">
  <link href="../assets/css/style-responsive.css" rel="stylesheet">

  
</head>


<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      
      <!--logo start-->
      <nav class="navbar navbar-expand-lg navbar-dark black-bg">
      <a class="logo navbar-brand" href="index.php"> <b>STUDENT SOCIAL <span>NETWORK</span></b></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>      <!--logo end-->
      
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <?php         
                if (isset($title) && strcmp($title,"Accueil")==0){
                 include("../parties/nav1.php"); }
                elseif(isset($title) && strcmp($title,"Inscription")==0){
                    include("../parties/nav3.php");}
                    elseif(isset($title) && strcmp($title,"Connexion")==0){
                        include("../parties/nav2.php");}
                        elseif(isset($title) && strcmp($title,"profils")==0){
                            include("../parties/nav4.php");}
                            elseif(isset($title) && (strcmp($title,"dashbord")==0 || strcmp($title,"cours_etd")==0 || strcmp($title,"note_etd")==0)){
                                include("../parties/nav5.php");} 
                                elseif(isset($title) && strcmp($title,"profil_ens")==0){
                                  include("../parties/nav6.php");}
                                  elseif(isset($title) && (strcmp($title,"dashbord_ens")==0 || strcmp($title,"cours_ens")==0 )){
                                    include("../parties/nav7.php");}?>
                </div>

</nav>


    </header>


      <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <?php if (isset($title) && strcmp($title,"profils")==0){
                 include("../parties/aside.php"); }
                 elseif(isset($title) && strcmp($title,"dashbord")==0){
                  include("../parties/aside.dash.php");}
                  elseif(isset($title) && strcmp($title,"cours_etd")==0){
                    include("../parties/aside.cours.php");}
                    elseif(isset($title) && strcmp($title,"note_etd")==0){
                      include("../parties/aside.note.php");}
                      elseif(isset($title) && strcmp($title,"message")==0){
                        include("../parties/aside.mess.php");}

                        if (isset($title) && strcmp($title,"profil_ens")==0){
                          include("../parties/aside1.php"); }
                          elseif(isset($title) && strcmp($title,"dashbord-ens")==0){
                           include("../parties/aside.dash_ens.php");}
                           elseif(isset($title) && strcmp($title,"cours_ens")==0){
                             include("../parties/aside.cours_ens.php");}
                               elseif(isset($title) && strcmp($title,"message_ens")==0){
                                 include("../parties/aside.mess_ens.php");}

                 ?>
    