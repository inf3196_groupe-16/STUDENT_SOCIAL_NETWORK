from django.db import connection,connections
from collections import namedtuple
from django.core.exceptions import ValidationError

def infos_by_id(id):
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT nom,prenom,matricule_etd,email,sexe,pays,ville,savoir_faire,date_nais,id_fil,id_niveau,photo_profil FROM etudiant WHERE id=%s",
            [id])
        user =namedtuplefetchall(cursor)
        return user
def infos_fil():
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT * FROM filiere")
        user =namedtuplefetchall(cursor)
        return user
def infos_ens_by_fil(fil):
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT matricule_ensg,prenom,nom FROM enseignant WHERE id_fil=%s",[fil])
        user =namedtuplefetchall(cursor)
        return user



def infos_by_id_ad(id):
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT nom,prenom,matricule_ad,id_fil,id_niveau FROM admin WHERE id=%s",
            [id])
        user =namedtuplefetchall(cursor)
        return user

def infos_by_mat_etd():
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT id,nom,prenom,matricule_etd,email,sexe,pays,ville,savoir_faire,date_nais,id_fil,id_niveau,photo_profil FROM etudiant")
        user =namedtuplefetchall(cursor)
        return user
def infos_by_mat_ens():
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT id,nom,prenom,matricule_ensg,email,id_fil,photo_profil FROM enseignant")
        user =namedtuplefetchall(cursor)
        return user



def infos_by_id_ens(id):
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT nom,prenom,matricule_ensg,email,date_nais,id_fil,photo_profil FROM enseignant WHERE id=%s",
            [id])
        user =namedtuplefetchall(cursor)
        return user
def find_etd(fil,niv,id):
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT nom,prenom,matricule_etd,id,photo_profil FROM etudiant WHERE id_fil=%s and id_niveau=%s and id<>%s and statut='1'",
            [fil,niv,id])
        user =namedtuplefetchall(cursor)
        return user

def infos_cd(cd):
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_niveau AS niv, id_fil AS fil FROM cours WHERE code=%s",[cd])
        user =namedtuplefetchall(cursor)
        return user

def namedtuplefetchall(cursor):
    desc=cursor.description
    nt_result=namedtuple('Result',[col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def handle_uploaded_file(f):
    with open('media/name.txt','wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

def validate_file_extension(value):
    if value.file.content_type != 'application/pdf' or 'text/csv' or 'text/html' or 'text/xml'  or 'audio/opus' or 'audio/mp4' or 'image/jpeg' or 'image/jpg' or 'image/png' or 'video/mp4':
        raise ValidationError(u'Error message')