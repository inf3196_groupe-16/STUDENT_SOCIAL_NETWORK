from django import forms
from django.forms import ModelForm
from .models import *

class LoginForm(forms.Form):
    username=forms.CharField(max_length=63 , label='nom d''utilisateur')
    password=forms.CharField(max_length=63, widget=forms.PasswordInput, label='mot de passe')

class UploadFileForm(forms.Form):
    img=forms.ImageField()
class modelformwithimgfield(ModelForm):
    class Meta:
        model=Image
        fields='__all__'

class modelformwithimgfield_ens(ModelForm):
    class Meta:
        model=Image_ens
        fields='__all__'

class modelformwithcours(ModelForm):
    class Meta:
        model=cours_doc
        fields='__all__'

class modelformwithpublic(ModelForm):
    class Meta:
        model=File_public
        fields='__all__'
