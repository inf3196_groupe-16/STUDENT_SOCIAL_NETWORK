
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from . import classes,fonction,models
from .forms import *
from django.http import JsonResponse
from django.contrib.auth.hashers import pbkdf2


# Create your views here.
def index(request):
    if 'user_id'  in request.session:
        data = {
            'id': request.session['user_id']
        }
        url = "ssn/edit_profil?id={}".format(request.session['user_id'])
        return HttpResponseRedirect(url)
    return render(request, 'pages/index.html')
def login(request):
    if 'user_id' in request.session:
        data = {
            'id': request.session['user_id']
        }
        url = "edit_profil?id={}".format(request.session['user_id'])
        return HttpResponseRedirect(url)
    else:
        if request.method == "POST":
            #traiter le formulaire
            username=request.POST.get("Username")
            password=request.POST.get("password")
            etd=classes.etudiant()
            res=etd.connecter(username,password,request)
            ens=classes.enseignant()
            res1=ens.connecter(username,password,request)
            ad = classes.admin()
            res2 = ad.connecter(username, password, request)
            print(res)
            if res1:
                data={
                    'id':request.session['user_id']
                }
                url="edit_profil_ens?id={}".format(request.session['user_id'])
                return HttpResponseRedirect(url)
            if res:
                data={
                    'id':request.session['user_id']
                }
                url="edit_profil?id={}".format(request.session['user_id'])
                return HttpResponseRedirect(url)
            if res2:
                data={
                    'id':request.session['user_id']
                }
                url="dashbord_ad?id={}".format(request.session['user_id'])
                return HttpResponseRedirect(url)
    return render(request, 'pages/login.html')

def edit_profil_ens(request):
    if 'user_id' not in request.session:
        return redirect('index')
    elif request.session['user_id']>0 and request.session['fonction']=='enseignant':
        if request.method == "GET":

            id=request.GET.get('id')
        if request.method=="POST":
            if 'send_pp' in request.POST:
                form=modelformwithimgfield_ens(request.POST,request.FILES)
                ens = classes.enseignant()
                res=ens.MAJ_pp(request.FILES['img'],request.session['user_id'])
                if res and form.is_valid():
                    form.save()
            else:
                form = modelformwithimgfield_ens()
        else:
            form = modelformwithimgfield_ens()

        if request.method == "POST":
            if 'Save_profil' in request.POST:
                Username=request.POST.get('Username')
                Prenom=request.POST.get('Prenom')
                Nom = request.POST.get('Nom')
                Email = request.POST.get('Email')
                Birthday = request.POST.get('Birthday')
                ens=classes.enseignant()
                res=ens.MAJ_info(request,Username,Nom,Prenom,Email,Birthday)
                if res:
                    redirect('edit_profil_ens')
            if 'Save_password' in request.POST:
                ens = classes.enseignant()
                newPassword=request.POST.get('newPassword')
                currentPassword = request.POST.get('currentPassword')
                confirmPassword = request.POST.get('confirmPassword')


                res = ens.MAJ_password(newPassword,currentPassword,confirmPassword,request.session['user_id'])

                if res:
                    redirect('edit_profil')

    users=fonction.infos_by_id_ens(request.session['user_id'])
    return render(request,'pages/edit_profil_ens.html',context={"form_img":form,"pp":users[0].photo_profil,"prenom": users[0].prenom,"nom":users[0].nom,"matricule":users[0].matricule_ensg,"email":users[0].email,"birthday":users[0].date_nais})


def edit_profil(request):
    if 'user_id' not in request.session:
        return redirect('index')
    elif request.session['user_id']>0 and request.session['fonction']=='etudiant':
        if request.method == "GET":

            id=request.GET.get('id')
        if request.method=="POST":
            form=modelformwithimgfield(request.POST,request.FILES)
            if 'send_pp' in request.POST:
                etd = classes.etudiant()
                res=etd.MAJ_pp(request.FILES['img'],request.session['user_id'])
                if res and form.is_valid():
                    form.save()

        else:
            form=modelformwithimgfield()
        if request.method == "POST":
            if 'Save_profil' in request.POST:
                Username=request.POST.get('Username')
                Prenom=request.POST.get('Prenom')
                Nom = request.POST.get('Nom')
                Sexe = request.POST.get('Sexe')
                Pays = request.POST.get('Pays')
                Ville = request.POST.get('Ville')
                Email = request.POST.get('Email')
                Birthday = request.POST.get('Birthday')
                A_propos = request.POST.get('A_propos')
                etd=classes.etudiant()
                res=etd.MAJ_info(request,Username,Nom,Prenom,Sexe,Pays,Ville,Email,Birthday,A_propos)
                if res:
                    redirect('edit_profil')
            if 'Save_password' in request.POST:
                etd = classes.etudiant()
                newPassword=request.POST.get('newPassword')
                currentPassword = request.POST.get('currentPassword')
                confirmPassword = request.POST.get('confirmPassword')


                res = etd.MAJ_password(newPassword,currentPassword,confirmPassword,request.session['user_id'])

                if res:
                    redirect('edit_profil')


    users=fonction.infos_by_id(request.session['user_id'])
    return render(request,'pages/edit_profil.html',context={"form_img":form,"pp":users[0].photo_profil,"prenom": users[0].prenom,"nom":users[0].nom,"matricule":users[0].matricule_etd,"sexe":users[0].sexe,"pays":users[0].pays,"ville":users[0].ville,"email":users[0].email,"birthday":users[0].date_nais,"a_propos":users[0].savoir_faire})
def dashbord(request):
    if 'user_id' not in request.session:
        return redirect('index')
    elif request.session['user_id']>0 and request.session['fonction']=='etudiant':
        users = fonction.infos_by_id(request.session['user_id'])
        if request.method == "GET":
            id = request.GET.get('id')
        new_post = classes.publication()
        comment = []
        commenteur = []
        posts = new_post.fil_actu_etd(request.session['niveau'],request.session['filiere'])

        comment1 = []
        posts1 = new_post.fil_actu_no_file_etd(request.session['niveau'],request.session['filiere'])

        total_like = []
        is_like = []
        nb_com=[]
        posteur=[]

        total_like1 = []
        is_like1 = []
        nb_com1=[]

        for i in posts:
            print(i.id_publi)
            comment.append(new_post.all_comment(i.id_publi))
            total_like.append(new_post.total_like(i.id_publi))
            is_like.append(new_post.is_like_etd(i.id_publi, request.session['Username']))
            nb_com.append(new_post.nb_comment(i.id_publi))

        for i in posts1:
            print(i.id_publi)
            comment1.append(new_post.all_comment(i.id_publi))
            total_like1.append(new_post.total_like(i.id_publi))
            is_like1.append(new_post.is_like_etd(i.id_publi, request.session['Username']))
            nb_com1.append(new_post.nb_comment(i.id_publi))

        if comment or comment1:
            commenteur.append(fonction.infos_by_mat_ens())
            commenteur.append(fonction.infos_by_mat_etd())
        posteur.append(fonction.infos_by_mat_etd())
        posteur.append(fonction.infos_by_mat_ens())
        print(posts1)
        if request.method=="POST":

            if 'comment' in request.POST:
                id_publication = request.POST.get('id_publication')
                print(id_publication)
                commentaire = request.POST.get('commentaire')
                etd = classes.publication()
                res=etd.commenter_etd(id_publication,users[0].matricule_etd,commentaire)
                if res :
                    redirect('dashbord')


    return render(request,'pages/dashbord.html',context={"posteur":posteur,"is_like1":is_like1,"total_like1":total_like1,"comment1":comment1,"nb_comment1":nb_com1,"posts1":posts1,"is_like":is_like,"total_like":total_like,"comment":comment,"nb_comment":nb_com,"commenteur":commenteur,"posts":posts,"pp":users[0].photo_profil,"prenom": users[0].prenom,"nom":users[0].nom,"matricule":users[0].matricule_etd})

def dashbord_ens(request):
    if 'user_id' not in request.session:
        return redirect('index')
    elif request.session['user_id']>0 and request.session['fonction']=='enseignant':
        users = fonction.infos_by_id_ens(request.session['user_id'])
        if request.method == "GET":
            id = request.GET.get('id')
        new_post = classes.publication()
        comment = []
        commenteur = []
        posts = new_post.fil_actu_ens(request.session['filiere'])

        comment1 = []
        posts1 = new_post.fil_actu_no_file_ens(request.session['filiere'])

        total_like = []
        is_like = []
        nb_com = []
        posteur=[]

        total_like1 = []
        is_like1 = []
        nb_com1 = []
        posteur1 = []

        for i in posts:
            comment.append(new_post.all_comment(i.id_publi))
            total_like.append(new_post.total_like(i.id_publi))
            is_like.append(new_post.is_like_etd(i.id_publi, request.session['Username']))
            nb_com.append(new_post.nb_comment(i.id_publi))



        for i in posts1:
            comment1.append(new_post.all_comment(i.id_publi))
            total_like1.append(new_post.total_like(i.id_publi))
            is_like1.append(new_post.is_like_etd(i.id_publi, request.session['Username']))
            nb_com1.append(new_post.nb_comment(i.id_publi))


        if comment or comment1:
            commenteur.append(fonction.infos_by_mat_ens())
            commenteur.append(fonction.infos_by_mat_etd())

        posteur.append(fonction.infos_by_mat_etd())
        posteur.append(fonction.infos_by_mat_ens())
        print(posts1)
        if request.method=="POST":

            if 'comment' in request.POST:
                id_publication = request.POST.get('id_publication')
                print(id_publication)
                commentaire = request.POST.get('commentaire')
                etd = classes.publication()
                res=etd.commenter_ens(id_publication,users[0].matricule_ensg,commentaire)
                if res :
                    redirect('dashbord_ens')
    return render(request,'pages/dashbord_ens.html',context={"posteur":posteur,"posteur1":posteur1,"is_like1":is_like1,"total_like1":total_like1,"comment1":comment1,"nb_comment1":nb_com1,"posts1":posts1,"is_like":is_like,"total_like":total_like,"comment":comment,"nb_comment":nb_com,"commenteur":commenteur,"posts":posts,"pp":users[0].photo_profil,"prenom": users[0].prenom,"nom":users[0].nom,"matricule":users[0].matricule_ensg})

def dashbord_ad(request):
    if 'user_id' not in request.session:
        return redirect('index')
    elif request.session['user_id']>0 and request.session['fonction']=='admin':
        users = fonction.infos_by_id_ad(request.session['user_id'])
        if request.method == "GET":
            id = request.GET.get('id')
        new_post = classes.publication()

        posts = new_post.fil_actu_ad(request.session['niveau'],request.session['filiere'])


        posts1 = new_post.fil_actu_no_file_ad(request.session['niveau'],request.session['filiere'])


        posteur=[]

        posteur.append(fonction.infos_by_mat_etd())
        print(posts1)
        print(posteur)
        if request.method=="POST":

            if 'accepter' in request.POST:
                id_publication = request.POST.get('id_publication')
                mat_etd=request.POST.get('mat_etd')
                print(id_publication)
                etd = classes.publication()
                res=etd.acccept_publi(id_publication,mat_etd)
                if res :
                    return redirect('dashbord_ad')
            if 'annuler' in request.POST:
                id_publication = request.POST.get('id_publication')
                mat_etd=request.POST.get('mat_etd')
                print(id_publication)
                etd = classes.publication()
                res=etd.not_acccept_publi(id_publication,mat_etd)
                if res :
                    return redirect('dashbord_ad')


    return render(request,'pages/dashbord_ad.html',context={"posteur":posteur,"posts1":posts1,"posts":posts,"prenom": users[0].prenom,"nom":users[0].nom,"matricule":users[0].matricule_ad})


def list_etd(request):
    if 'user_id' not in request.session:
        return redirect('index')
    elif request.session['user_id']>0 and request.session['fonction']=='etudiant':
        if request.method == "GET":

            id=request.GET.get('id')
    res=fonction.find_etd(request.session['filiere'],request.session['niveau'],request.session['user_id'])
    users = fonction.infos_by_id(request.session['user_id'])
    return render(request,'pages/list_etd.html',context={"res":res,"pp":users[0].photo_profil,"prenom": users[0].prenom,"nom":users[0].nom,"matricule":users[0].matricule_etd})

def profil_public(request):
    if 'user_id' not in request.session:
        return redirect('index')
    elif request.session['user_id']>0 and request.session['fonction']=='etudiant':
        if request.method == "GET":
            print(request.GET.get('mat'))
            print(request.session['Username'])
            if request.GET.get('mat') == request.session['Username']:
                return HttpResponseRedirect("profil?mat={}".format(request.session['Username']))

        id = request.GET.get('mat')
        if id == request.session['Username']:
            print(True)
        users1 = fonction.infos_by_id(id)
        ami=classes.amitie()
        status=ami.status_demande(id,request.session['Username'])
        status1 = ami.status_demande(request.session['Username'],id)
        print(status)
        print(status1)

        if request.method=="POST":
            if 'demander' in request.POST:
                res=ami.demander(request.session['Username'],id)
            elif 'annuler' in request.POST:
                res=ami.refuser(request.session['Username'],id)
            elif 'refuser' in request.POST:
                res=ami.refuser(id,request.session['Username'])
            elif 'supprimer' in request.POST:
                if status:
                    res=ami.refuser(id,request.session['Username'])
                if status1:
                    res=ami.refuser(request.session['Username'],id)
            if res=='1' or res=='0' or res==3:
                url = "profil_public?id={}".format(id)
                return HttpResponseRedirect(url)

    users = fonction.infos_by_id(request.session['user_id'])
    return render(request,'pages/profil_public.html',context={"status1":status1,"status":status,"pp":users[0].photo_profil,"prenom": users[0].prenom,"nom":users[0].nom,"matricule":users[0].matricule_etd,"pp_p":users1[0].photo_profil,"prenom_p":users1[0].prenom,"nom_p":users1[0].nom,"savoir_faire_p":users1[0].savoir_faire,"ville_p":users1[0].ville,"pays_p":users1[0].pays,"email_p":users1[0].email})



def profil(request):

    if 'user_id' not in request.session:
        return redirect('index')
    elif request.session['user_id']>0 and request.session['fonction']=='etudiant':
        users = fonction.infos_by_id(request.session['user_id'])
        new = classes.cours()
        cours = new.find_by_cd(users[0].id_niveau, users[0].id_fil)
        if request.method == "GET":

            id=request.GET.get('id')
        new_post=classes.publication()
        comment=[]
        commenteur=[]
        posts=new_post.all_publi_etd(request.session['Username'])

        comment1=[]
        posts1=new_post.all_publi_no_file_etd(request.session['Username'])

        total_like=[]
        total_unlike=[]
        is_like=[]
        nb_com=[]

        total_like1 = []
        total_unlike1 = []
        is_like1 = []
        nb_com1=[]

        for i in posts:
            print(i.id_publi)
            comment.append(new_post.all_comment(i.id_publi))
            total_like.append(new_post.total_like(i.id_publi))
            is_like.append(new_post.is_like_etd(i.id_publi, request.session['Username']))
            nb_com.append(new_post.nb_comment(i.id_publi))

        for i in posts1:
            print(i.id_publi)
            comment1.append(new_post.all_comment(i.id_publi))
            total_like1.append(new_post.total_like(i.id_publi))
            is_like1.append(new_post.is_like_etd(i.id_publi, request.session['Username']))
            nb_com1.append(new_post.nb_comment(i.id_publi))
        print(comment)

        if comment or comment1:
            commenteur.append(fonction.infos_by_mat_ens())
            commenteur.append(fonction.infos_by_mat_etd())


        if request.method=="POST":

            form=modelformwithpublic(request.POST,request.FILES)
            if 'publier' in request.POST:
                titre = request.POST.get('titre')
                descriptif = request.POST.get('description')
                cour = request.POST.get('code')
                info_cour = fonction.infos_cd(cour)
                niv = info_cour[0].niv
                fil = info_cour[0].fil
                etd = classes.publication()
                if 'publication' in request.FILES:
                    file = request.FILES['publication']
                    print(file.content_type)
                    res=etd.save_publi_etd(titre,descriptif,niv,fil,file,users[0].matricule_etd,cour)
                    if res and form.is_valid():
                        form.save()
                        redirect('profil')
                else:
                    res1 = etd.save_publi_etd_no_file(titre, descriptif, niv, fil, users[0].matricule_etd, cour)
                    if res1 :
                        redirect('profil')



            if 'comment' in request.POST:
                id_publication = request.POST.get('id_publication')
                print(id_publication)
                commentaire = request.POST.get('commentaire')
                etd = classes.publication()
                res=etd.commenter_etd(id_publication,users[0].matricule_etd,commentaire)
                if res :
                    redirect('profil')

        else:
            form=modelformwithpublic()


    return render(request,'pages/profil.html',context={"is_like1":is_like1,"total_like1":total_like1,"total_unlike1":total_unlike1,"comment1":comment1,"nb_comment1":nb_com1,"posts1":posts1,"is_like":is_like,"total_like":total_like,"total_unlike":total_unlike,"comment":comment,"nb_comment":nb_com,"commenteur":commenteur,"posts":posts,"form":form,"cours":cours,"savoir_faire":users[0].savoir_faire,"pp":users[0].photo_profil,"prenom": users[0].prenom,"nom":users[0].nom,"matricule":users[0].matricule_etd})

def profil_ens(request):
    if 'user_id' not in request.session:
        return redirect('index')
    elif request.session['user_id']>0 and request.session['fonction']=='enseignant':
        users = fonction.infos_by_id_ens(request.session['user_id'])
        new = classes.cours()
        cours = new.find_by_ens(request.session['Username'])
        if request.method == "GET":
            id = request.GET.get('id')
        new_post = classes.publication()
        comment = []
        commenteur = []
        posts = new_post.all_publi_ens(request.session['Username'])

        comment1 = []
        posts1 = new_post.all_publi_no_file_ens(request.session['Username'])

        total_like = []
        total_unlike = []
        is_like = []
        nb_com = []

        total_like1 = []
        total_unlike1 = []
        is_like1 = []
        nb_com1 = []
        for i in posts:
            comment.append(new_post.all_comment(i.id_publi))
            total_like.append(new_post.total_like(i.id_publi))
            is_like.append(new_post.is_like_etd(i.id_publi, request.session['Username']))
            nb_com.append(new_post.nb_comment(i.id_publi))

        for i in posts1:
            comment1.append(new_post.all_comment(i.id_publi))
            total_like1.append(new_post.total_like(i.id_publi))
            is_like1.append(new_post.is_like_etd(i.id_publi, request.session['Username']))
            nb_com1.append(new_post.nb_comment(i.id_publi))
        print(posts1)

        if comment or comment1:
            commenteur.append(fonction.infos_by_mat_ens())
            commenteur.append(fonction.infos_by_mat_etd())

        if request.method == "POST":

            form = modelformwithpublic(request.POST, request.FILES)
            if 'publier' in request.POST:
                titre = request.POST.get('titre')
                descriptif = request.POST.get('description')
                cour = request.POST.get('code')
                info_cour = fonction.infos_cd(cour)
                niv = info_cour[0].niv
                fil = info_cour[0].fil
                etd = classes.publication()
                if 'publication' in request.FILES:
                    file = request.FILES['publication']
                    print(file.content_type)
                    res = etd.save_publi_ens(titre, descriptif, niv, fil, file, users[0].matricule_ensg, cour)
                    if res and form.is_valid():
                        form.save()
                        redirect('profil')
                else:
                    res1 = etd.save_publi_ens_no_file(titre, descriptif, niv, fil, users[0].matricule_ensg, cour)
                    if res1:
                        redirect('profil')

            if 'comment' in request.POST:
                id_publication = request.POST.get('id_publication')
                print(id_publication)
                commentaire = request.POST.get('commentaire')
                etd = classes.publication()
                res = etd.commenter_ens(id_publication, users[0].matricule_ensg, commentaire)
                if res:
                    redirect('profil')

        else:
            form = modelformwithpublic()
    return render(request,'pages/profil_ens.html',context={"is_like1":is_like1,"total_like1":total_like1,"total_unlike1":total_unlike1,"comment1":comment1,"nb_comment1":nb_com1,"posts1":posts1,"is_like":is_like,"total_like":total_like,"total_unlike":total_unlike,"comment":comment,"nb_comment":nb_com,"commenteur":commenteur,"posts":posts,"form":form,"cours":cours,"pp":users[0].photo_profil,"prenom": users[0].prenom,"nom":users[0].nom,"matricule":users[0].matricule_ensg})


def notes(request):
    if 'user_id' not in request.session:
        return redirect('index')
    elif request.session['user_id']>0 and request.session['fonction']=='etudiant':
        if request.method == "GET":

            id=request.GET.get('id')

        users = fonction.infos_by_id(request.session['user_id'])
        cr = classes.babibllard()
        res=cr.set_notes(users[0].matricule_etd,'1')
        res1=cr.set_notes(users[0].matricule_etd,'2')

    return render(request,'pages/note_etd.html',context={"res":res,"res1":res1,"savoir_faire":users[0].savoir_faire,"pp":users[0].photo_profil,"prenom": users[0].prenom,"nom":users[0].nom,"matricule":users[0].matricule_etd})

def cours_etd(request):
    if 'user_id' not in request.session:
        return redirect('index')
    elif request.session['user_id']>0 and request.session['fonction']=='etudiant':
        if request.method == "GET":

            id=request.GET.get('id')
        cr=classes.cours()
        res = cr.find_by_cd(request.session['niveau'], request.session['filiere'])
        n=len(res)
        print(n)
        res1=[]
        for i in res:
            res1.append(cr.find_file_txt(request.session['niveau'], request.session['filiere'],i.code))


    users = fonction.infos_by_id(request.session['user_id'])
    return render(request,'pages/cours_etd.html',context={"res":res,"res1":res1,"savoir_faire":users[0].savoir_faire,"pp":users[0].photo_profil,"prenom": users[0].prenom,"nom":users[0].nom,"matricule":users[0].matricule_etd})

def cours_ens(request):
    if 'user_id' not in request.session:
        return redirect('index')
    elif request.session['user_id']>0 and request.session['fonction']=='enseignant':
        if request.method == "GET":
            id=request.GET.get('id')
        users = fonction.infos_by_id_ens(request.session['user_id'])
        ens = classes.cours()
        res=ens.find_by_ens(request.session['Username'])
        if request.method == "POST":
            form = modelformwithcours(request.POST, request.FILES)
            code = request.POST.get("code")
            description  = request.POST.get("description")
            nf=fonction.infos_cd(code)

            res1 = ens.poster(code,description,request.FILES['cours'],nf[0].niv,nf[0].fil)
            if res1 and form.is_valid():
                form.save()

        else:
            form = modelformwithcours()
    return render(request,'pages/cours_ens.html',context={"form_file":form,"res":res,"pp":users[0].photo_profil,"prenom": users[0].prenom,"nom":users[0].nom,"matricule":users[0].matricule_ensg})


def deconnexion(request):
    if request.session.clear():
        return HttpResponseRedirect("index")
    return render(request,'pages/index.html')

def like_post(request):
    if request.method=='POST':
        id_publication=request.POST.get('id_publication')
        user=request.session['Username']
        new_like=classes.publication()
        is_like=new_like.is_like_etd(id_publication,user)
        all_like=new_like.total_like(id_publication)
        if is_like:
            new_like.del_like(id_publication,user)
        elif not is_like:
            new_like.like(id_publication,user)
        data={
            'statut':is_like,
            'likes':all_like[1]

        }
        return JsonResponse(data,safe=False)

    return redirect('profil')
def like_fil_actu(request):
    if request.method=='POST':
        id_publication=request.POST.get('id_publication')
        user=request.session['Username']
        new_like=classes.publication()
        is_like=new_like.is_like_etd(id_publication,user)
        all_like=new_like.total_like(id_publication)
        if is_like:
            new_like.del_like(id_publication,user)
        elif not is_like:
            new_like.like(id_publication,user)
        data={
            'statut':is_like,
            'likes':all_like[1]

        }
        return JsonResponse(data,safe=False)

    return redirect('dashbord')

def like_post_ens(request):
    if request.method=='POST':
        id_publication=request.POST.get('id_publication')
        user=request.session['Username']
        new_like=classes.publication()
        is_like=new_like.is_like_etd(id_publication,user)
        all_like=new_like.total_like(id_publication)
        if is_like:
            new_like.del_like(id_publication,user)
        elif not is_like:
            new_like.like(id_publication,user)
        data={
            'statut':is_like,
            'likes':all_like[1]

        }
        return JsonResponse(data,safe=False)

    return redirect('profil_ens')
def like_fil_actu_ens(request):
    if request.method=='POST':
        id_publication=request.POST.get('id_publication')
        user=request.session['Username']
        new_like=classes.publication()
        is_like=new_like.is_like_etd(id_publication,user)
        all_like=new_like.total_like(id_publication)
        if is_like:
            new_like.del_like(id_publication,user)
        elif not is_like:
            new_like.like(id_publication,user)
        data={
            'statut':is_like,
            'likes':all_like[1]

        }
        return JsonResponse(data,safe=False)

    return redirect('dashbord_ens')

def gestion_ens(request):
    if 'user_id' not in request.session:
        return redirect('index')
    elif request.session['user_id']>0 and request.session['fonction']=='admin':
        if request.method == "GET":

            id=request.GET.get('id')
        fil=fonction.infos_fil()
        ensg=fonction.infos_ens_by_fil(request.session['filiere'])
        if request.method=="POST":
            if 'Save_ens' in request.POST:
                ens = classes.enseignant()
                Username = request.POST.get('Username')
                Prenom = request.POST.get('Prenom')
                Nom = request.POST.get('Nom')
                filiere=request.POST.get('filiere')
                Email = request.POST.get('Email')
                Birthday = request.POST.get('Birthday')
                newPassword = request.POST.get('newPassword')
                confirmPassword = request.POST.get('confirmPassword')

                res = ens.ajout_ens(request, Username, Nom, Prenom, Email, Birthday,newPassword, confirmPassword, filiere)

                if res :
                    return redirect('gestion_ens')

            if 'Save_cours' in request.POST:
                ens = classes.cours()
                Username = request.POST.get('mat_ensg')
                code = request.POST.get('code')
                descip = request.POST.get('descrip')
                filiere=request.POST.get('filiere')
                niveau = request.POST.get('niveau')
                res = ens.ajouter(code,descip,niveau,filiere,Username)
                if res :
                    return redirect('gestion_ens')



    users=fonction.infos_by_id_ad(request.session['user_id'])
    return render(request,'pages/gestion_ens.html',context={"ensg":ensg,"cours":fil,"prenom": users[0].prenom,"nom":users[0].nom,"matricule":users[0].matricule_ad})

def message(request):
    if 'user_id' not in request.session:
        return redirect('index')
    elif request.session['user_id']>0 and request.session['fonction']=='etudiant':
        users = fonction.infos_by_id(request.session['user_id'])



    return render(request,'pages/message.html',context={"savoir_faire":users[0].savoir_faire,"pp":users[0].photo_profil,"prenom": users[0].prenom,"nom":users[0].nom,"matricule":users[0].matricule_etd})
