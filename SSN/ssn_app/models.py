# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from .fonction import validate_file_extension


class Acceder(models.Model):
    matricule_etd = models.ForeignKey('Etudiant', models.DO_NOTHING, db_column='matricule_etd')
    code = models.OneToOneField('Cours', models.DO_NOTHING, db_column='code', primary_key=True)

    class Meta:
        db_table = 'acceder'
        unique_together = (('code', 'matricule_etd'),)


class Adresse(models.Model):
    id_adresse = models.IntegerField(primary_key=True)
    nom_adresse = models.CharField(max_length=25, blank=True, null=True)

    class Meta:
        db_table = 'adresse'


class Commentaire(models.Model):
    id_com = models.IntegerField(primary_key=True)
    matricule_editeur = models.CharField(max_length=15, blank=True, null=True)
    id_publication = models.ForeignKey('Publication', models.DO_NOTHING, db_column='id_publication', blank=True,
                                       null=True)
    date_comment = models.DateField(blank=True, null=True)
    contenu = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        db_table = 'commentaire'


class Cours(models.Model):
    code = models.CharField(primary_key=True, max_length=10)
    descrisption = models.CharField(max_length=50, blank=True, null=True)
    date_diff = models.DateField(blank=True, null=True)
    id_niveau = models.ForeignKey('Niveau', models.DO_NOTHING, db_column='id_niveau', blank=True, null=True)
    id_fil = models.ForeignKey('Filiere', models.DO_NOTHING, db_column='id_fil', blank=True, null=True)
    matricule_ensg = models.ForeignKey('Enseignant', models.DO_NOTHING, db_column='matricule_ensg', blank=True,
                                       null=True)

    class Meta:
        db_table = 'cours'


class Enseignant(models.Model):
    matricule_ensg = models.CharField(primary_key=True, max_length=10)
    email = models.CharField(max_length=25)
    nom = models.CharField(max_length=25)
    password = models.CharField(db_column='PASSWORD', max_length=50)  # Field name made lowercase.
    prenom = models.CharField(max_length=25, blank=True, null=True)
    date_nais = models.DateField(blank=True, null=True)
    id_fil = models.ForeignKey('Filiere', models.DO_NOTHING, db_column='id_fil', blank=True, null=True)
    id_adresse = models.ForeignKey(Adresse, models.DO_NOTHING, db_column='id_adresse', blank=True, null=True)
    photo_profil = models.CharField(max_length=25, blank=True, null=True)

    class Meta:
        db_table = 'enseignant'
        unique_together = (('matricule_ensg', 'email'),)


class Etudiant(models.Model):
    matricule_etd = models.CharField(primary_key=True, max_length=9)
    nom = models.CharField(max_length=25, blank=True, null=True)
    prenom = models.CharField(max_length=25, blank=True, null=True)
    date_nais = models.DateField(blank=True, null=True)
    id_fil = models.ForeignKey('Filiere', models.DO_NOTHING, db_column='id_fil', blank=True, null=True)
    id_niveau = models.ForeignKey('Niveau', models.DO_NOTHING, db_column='id_niveau', blank=True, null=True)
    id_adresse = models.ForeignKey(Adresse, models.DO_NOTHING, db_column='id_adresse', blank=True, null=True)
    statut = models.CharField(max_length=1)
    photo_profil = models.CharField(max_length=50, blank=True, null=True)
    password = models.CharField(max_length=50)
    sexe = models.CharField(max_length=1)
    pays = models.CharField(max_length=50)
    ville = models.CharField(max_length=50)
    savoir_faire = models.CharField(max_length=250)
    email = models.CharField(unique=True, max_length=50)
    class Meta:
        db_table = 'etudiant'

class Image(models.Model):
    img=models.ImageField(upload_to="photo_profil",blank=True,null=True)
    class Meta:
        db_table='ssn_app_image'
class Image_ens(models.Model):
    img=models.ImageField(upload_to="photo_profil_ens",blank=True,null=True)
    class Meta:
        db_table='ssn_app_image_ens'
class cours_doc(models.Model):
    cours=models.FileField(upload_to="cours",blank=True,null=True)
    class Meta:
        db_table='ssn_app_cours_doc'

class File_public(models.Model):
    publication=models.FileField(upload_to="publication",blank=True,null=True)
    class Meta:
        db_table='ssn_app_file_public'


class Fichier(models.Model):
    id_fichier = models.AutoField(primary_key=True)
    descriptif = models.CharField(max_length=25)
    contenu = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        db_table = 'fichier'


class Filiere(models.Model):
    id_fil = models.CharField(primary_key=True, max_length=10)
    nom_fil = models.CharField(max_length=25, blank=True, null=True)

    class Meta:
        db_table = 'filiere'


class Lier(models.Model):
    code = models.OneToOneField(Cours, models.DO_NOTHING, db_column='code', primary_key=True)
    id_fichier = models.ForeignKey(Fichier, models.DO_NOTHING, db_column='id_fichier')
    id_type = models.ForeignKey('TypeFichier', models.DO_NOTHING, db_column='id_type')
    date_publication = models.DateTimeField()

    class Meta:
        db_table = 'lier'
        unique_together = (('code', 'id_fichier', 'id_type'),)


class Message(models.Model):
    id_message = models.IntegerField(primary_key=True)
    contenu = models.CharField(max_length=255, blank=True, null=True)
    date = models.DateField(blank=True, null=True)
    matricule_etd_envoi = models.ForeignKey(Etudiant, models.DO_NOTHING, db_column='matricule_etd_envoi', blank=True,
                                            null=True,related_name='mat_env_mess')
    matricule_etd_desti = models.ForeignKey(Etudiant, models.DO_NOTHING, db_column='matricule_etd_desti', blank=True,
                                            null=True,related_name='mat_dest_mess')

    class Meta:
        db_table = 'message'


class Niveau(models.Model):
    id_niveau = models.AutoField(primary_key=True)
    nom_niveau = models.CharField(max_length=25, blank=True, null=True)

    class Meta:
        db_table = 'niveau'


class Note(models.Model):
    id_note = models.AutoField(primary_key=True)
    code = models.ForeignKey(Cours, models.DO_NOTHING, db_column='code')
    matricule_etd = models.ForeignKey(Etudiant, models.DO_NOTHING, db_column='matricule_etd')
    note_cc = models.FloatField(blank=True, null=True)
    note_tp = models.FloatField(blank=True, null=True)
    note_sn = models.FloatField(blank=True, null=True)
    id_sem = models.ForeignKey('Semestre', models.DO_NOTHING, db_column='id_sem', blank=True, null=True)

    class Meta:
        db_table = 'note'


class Publication(models.Model):
    id_publication = models.AutoField(primary_key=True)
    matricule_ensg = models.ForeignKey(Enseignant, models.DO_NOTHING, db_column='matricule_ensg', blank=True, null=True)
    date_pub = models.DateField(blank=True, null=True)
    id_fichier = models.ForeignKey(Fichier, models.DO_NOTHING, db_column='id_fichier', blank=True, null=True)
    id_fil = models.ForeignKey(Filiere, models.DO_NOTHING, db_column='id_fil', blank=True, null=True)
    id_niveau = models.ForeignKey(Niveau, models.DO_NOTHING, db_column='id_niveau', blank=True, null=True)

    class Meta:
        db_table = 'publication'


class Semestre(models.Model):
    id_sem = models.AutoField(primary_key=True)
    nom_sem = models.CharField(max_length=55)

    class Meta:
        db_table = 'semestre'


class TypeFichier(models.Model):
    id_type = models.AutoField(primary_key=True)
    nom_type = models.CharField(max_length=25, blank=True, null=True)

    class Meta:
        db_table = 'type_fichier'
