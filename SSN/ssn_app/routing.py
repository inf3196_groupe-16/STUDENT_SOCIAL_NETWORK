from django.urls import path
from . import consumers


websocket_urlpatterns = [
    path('ssn/message', consumers.ChatConsumer.as_asgi()),
]