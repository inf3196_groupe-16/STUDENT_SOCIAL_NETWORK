import os

from django.db import connection,connections
from collections import namedtuple
from django.shortcuts import redirect
from django.contrib import messages
import hashlib,shutil
import os
from .forms import UploadFileForm,modelformwithimgfield
from django.core.files.uploadhandler import TemporaryUploadedFile


class admin():
    def namedtuplefetchall(self,cursor):
        desc=cursor.description
        nt_result=namedtuple('Result',[col[0] for col in desc])
        return [nt_result(*row) for row in cursor.fetchall()]

    def connecter(self, username, password, request):
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT id,matricule_ad,id_fil,id_niveau FROM admin WHERE matricule_ad=%s AND password=%s",
                [username, password])
            user = self.namedtuplefetchall(cursor)
            count = len(user)

        if count:
            print(user)
            request.session['user_id'] = user[0].id
            request.session['Username'] = user[0].matricule_ad
            request.session['filiere'] = user[0].id_fil
            request.session['niveau'] = user[0].id_niveau
            request.session['fonction'] = 'admin'
            return True
        else:
            messages.add_message(request, messages.WARNING, 'identifiant ou mot de passe incorrect')

class enseignant():
    def namedtuplefetchall(self,cursor):
        desc=cursor.description
        nt_result=namedtuple('Result',[col[0] for col in desc])
        return [nt_result(*row) for row in cursor.fetchall()]

    def ajout_ens(self,request,Username,Nom,Prenom,Email,Birthday,newPassword,confirmPassword,fil):
        errors = []
        if len(Username) < 6:
            errors.append("nom d'utilisateur trop court (minimum 6 caracteres)")
        if self.is_already_in_profil('email', Email, 'enseignant', request.session['Username']):
            errors.append('email deja utilise')
        if self.is_already_in_profil('matricule_ensg', Username, 'enseignant', request.session['user_id']):
            errors.append('Matricule deja utilise')
        if len(newPassword) < 8:
            errors.append("password trop court (minimum 8 caracteres)")
            print(errors)
        else:

            if (newPassword != confirmPassword):
                errors.append("mots de passes non identiques")
                print(errors)
            else:
                print("mots de passes identiaues")

        if len(errors) == 0:
            with connection.cursor() as cursor:
                cursor.execute(
                    "INSERT INTO enseignant (nom,prenom,matricule_ensg,email,date_nais,PASSWORD,id_fil) VALUES(%s,%s,%s,%s,%s,%s,%s)",
                    [Nom, Prenom, Username, Email, Birthday, newPassword,fil])


        return True
    def connecter(self, username, password, request):
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT id,matricule_ensg,id_fil FROM enseignant WHERE (matricule_ensg=%s OR email=%s) AND PASSWORD=%s",
                [username, username, password])
            user = self.namedtuplefetchall(cursor)
            count = len(user)

        if count:
            print(user)
            request.session['user_id'] = user[0].id
            request.session['Username'] = user[0].matricule_ensg
            request.session['filiere'] = user[0].id_fil
            request.session['fonction'] = 'enseignant'
            return True
        else:
            messages.add_message(request, messages.WARNING, 'identifiant ou mot de passe incorrect')
    def is_already_in_profil(self,field,value,table,id):
        with connection.cursor() as cursor:
            cursor.execute("SELECT id FROM etudiant WHERE matricule_etd<>%s AND %s=%s",[id,field,value])
            user = self.namedtuplefetchall(cursor)
            count = len(user)
            return count
        with connection.cursor() as cursor:
            cursor.execute("SELECT id FROM enseignant WHERE matricule_ensg<>%s  AND %s=%s",[id,field,value])
            user = self.namedtuplefetchall(cursor)
            count = len(user)
            return count

    def MAJ_info(self,request,Username,Nom,Prenom,Email,Birthday):
        errors = []
        if len(Username)<6:
            errors.append("nom d'utilisateur trop court (minimum 6 caracteres)")
        if self.is_already_in_profil('email',Email,'enseignant',request.session['Username']):
            errors.append('email deja utilise')
        if self.is_already_in_profil('matricule_ensg',Username,'enseignant',request.session['user_id']):
            errors.append('Matricule deja utilise')
        if len(errors)==0:
            with connection.cursor() as cursor:
                cursor.execute(
                    "UPDATE enseignant SET nom=%s,prenom=%s,matricule_ensg=%s,email=%s,date_nais=%s WHERE id=%s",
                    [Nom,Prenom,Username,Email,Birthday,request.session['user_id']])
                return True

    def MAJ_pp(self,file,id):
        max_size=3*1024*1024
        validateextensions=[".jpeg",".jpg"]
        dest="/media/photo_profil_ens/"
        fname,extension=os.path.splitext(file.name)


        if (file.content_type=="image/jpg" or file.content_type=="image/jpeg") and extension in validateextensions:
            if file.size<max_size:
                    print("pas d'erreur")
                    file.name=str(id)+extension
                    if os.path.exists("media/photo_profil_ens/"+file.name):
                        os.remove("media/photo_profil_ens/"+file.name)
                    with connection.cursor() as cursor:
                        cursor.execute(
                            "UPDATE enseignant SET photo_profil=%s WHERE id=%s",
                            [dest+file.name,id])
                    return True
    def MAJ_password(self,newPassword,currentPassword,confirmPassword,id):
        errors=[]
        if len(newPassword)<8:
            errors.append("password trop court (minimum 8 caracteres)")
            print(errors)
        else:
            # currentPassword = pbkdf2(currentPassword, 100, 10000)
            #newPassword = pbkdf2(newPassword.encode("utf-8"),10,10)
            #confirmPassword = pbkdf2(confirmPassword.encode("utf-8"),10,10)

            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT id FROM enseignant WHERE id=%s AND password=%s",[id,currentPassword])
                user = self.namedtuplefetchall(cursor)
                count = len(user)

            if(newPassword != confirmPassword):
                errors.append("mots de passes non identiques")
                print(errors)
            else:
                print("mots de passes identiaues")
                if count and len(errors)==0:
                    print("pas d'erreurs")
                    with connection.cursor() as cursor:
                        cursor.execute(
                            "UPDATE enseignant SET PASSWORD=%s WHERE id=%s", [newPassword, id])
                    return True

class etudiant():
    connection_params = {
        'host': "localhost",
        'user': "root",
        'password': "",
        'database': "ssn",
    }
    def namedtuplefetchall(self,cursor):
        desc=cursor.description
        nt_result=namedtuple('Result',[col[0] for col in desc])
        return [nt_result(*row) for row in cursor.fetchall()]
    def connecter(self,username,password,request):
            #password=password.encode('utf-8')
            #password=hashlib.sha1(password)
            with connection.cursor() as cursor:
                cursor.execute("SELECT id,matricule_etd,id_fil,id_niveau FROM etudiant WHERE (matricule_etd=%s OR email=%s) AND statut='1' AND password=%s",[username,username,password])
                user = self.namedtuplefetchall(cursor)
                count=len(user)


            if count:
                print(user)
                password = password.encode('utf-8')
                password=hashlib.sha1(password)
                print(password)
                request.session['user_id']=user[0].id
                request.session['Username'] =user[0].matricule_etd
                request.session['niveau'] =user[0].id_niveau
                request.session['filiere'] =user[0].id_fil
                request.session['fonction'] = 'etudiant'
                return True
            else:
                messages.add_message(request,messages.WARNING,'identifiant ou mot de passe incorrect')


    def MAJ_pp(self,file,id):
        max_size=3*1024*1024
        validateextensions=[".jpeg",".jpg"]
        dest="/media/photo_profil/"
        fname,extension=os.path.splitext(file.name)

        if (file.content_type=="image/jpg" or file.content_type=="image/jpeg") and extension in validateextensions:
            if file.size<max_size:
                    print("pas d'erreur")
                    file.name=str(id)+extension
                    if os.path.exists("media/photo_profil/"+file.name):
                        os.remove("media/photo_profil/"+file.name)
                    with connection.cursor() as cursor:
                        cursor.execute(
                            "UPDATE etudiant SET photo_profil=%s WHERE id=%s",
                            [dest+file.name,id])
                    return True




    def is_already_in_profil(self,field,value,table,id):
        with connection.cursor() as cursor:
            cursor.execute("SELECT id FROM etudiant WHERE matricule_etd<>%s AND %s=%s",[id,field,value])
            user = self.namedtuplefetchall(cursor)
            count = len(user)
            return count
        with connection.cursor() as cursor:
            cursor.execute("SELECT id FROM enseignant WHERE matricule_ensg<>%s  AND %s=%s",[id,field,value])
            user = self.namedtuplefetchall(cursor)
            count = len(user)
            return count
    def MAJ_info(self,request,Username,Nom,Prenom,Sexe,Pays,Ville,Email,Birthday,A_propos):
        errors = []
        if len(Username)<6:
            errors.append("nom d'utilisateur trop court (minimum 6 caracteres)")
        if self.is_already_in_profil('email',Email,'etudiant',request.session['Username']):
            errors.append('email deja utilise')
        if self.is_already_in_profil('matricule_etd',Username,'etudiant',request.session['user_id']):
            errors.append('Matricule deja utilise')
        if len(errors)==0:
            with connection.cursor() as cursor:
                cursor.execute(
                    "UPDATE etudiant SET nom=%s,prenom=%s,matricule_etd=%s,sexe=%s,pays=%s,ville=%s,email=%s,date_nais=%s,savoir_faire=%s WHERE id=%s",
                    [Nom,Prenom,Username,Sexe,Pays,Ville,Email,Birthday,A_propos,request.session['user_id']])
                return True

    def MAJ_password(self,newPassword,currentPassword,confirmPassword,id):
        errors=[]
        if len(newPassword)<8:
            errors.append("password trop court (minimum 8 caracteres)")
            print(errors)
        else:
            # currentPassword = pbkdf2(currentPassword, 100, 10000)
            #newPassword = pbkdf2(newPassword.encode("utf-8"),10,10)
            #confirmPassword = pbkdf2(confirmPassword.encode("utf-8"),10,10)

            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT id FROM etudiant WHERE id=%s AND password=%s",[id,currentPassword])
                user = self.namedtuplefetchall(cursor)
                count = len(user)
                print(id)
                print(currentPassword)
                print(count)
            if(newPassword != confirmPassword):
                errors.append("mots de passes non identiques")
                print(errors)
            else:
                print(";ots de passes identiaues")
                if count and len(errors)==0:
                    print("pas d'erreurs")
                    with connection.cursor() as cursor:
                        cursor.execute(
                            "UPDATE etudiant SET password=%s WHERE id=%s", [newPassword, id])
                    return True
class cours:
    def namedtuplefetchall(self,cursor):
        desc=cursor.description
        nt_result=namedtuple('Result',[col[0] for col in desc])
        return [nt_result(*row) for row in cursor.fetchall()]
    def poster(self,code_cours,descriptif,fichier,niv,fil):
        dest="/media/cours/"
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT fichier.id_fichier FROM cours,fichier,type_fichier,lier WHERE cours.code=lier.code AND fichier.id_fichier=lier.id_fichier AND lier.id_type AND cours.id_niveau=%s AND cours.id_fil=%s AND type_fichier.nom_type=%s AND cours.code=%s AND fichier.contenu=%s ", [niv,fil,'texte',code_cours,fichier])
            user = self.namedtuplefetchall(cursor)
            count = len(user)
        if count==0:
            with connection.cursor() as cursor:
                cursor.execute(
                    "INSERT INTO fichier(contenu,descriptif) VALUES (%s,%s) ",
                    [dest+fichier.name,descriptif])
                rs = cursor.lastrowid
            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT id_type FROM type_fichier WHERE nom_type='texte'")
                type = self.namedtuplefetchall(cursor)
            with connection.cursor() as cursor:
                cursor.execute(
                    "INSERT INTO lier(code,id_fichier,id_type) VALUES (%s,%s,%s)",
                    [code_cours,rs,type[0].id_type])
                return True

    def telecharger(self,code_cours):
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT fichier.contenu AS lien FROM fichier,type_fichier WHERE type_fichier.nom_fichier=%s AND code_cours=%s",
                ['texte', code_cours])
            user = self.namedtuplefetchall(cursor)
            return user[0].lien
    def find_by_cd(self,niv,fil):
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT * FROM cours WHERE id_niveau=%s AND id_fil=%s",
                [niv, fil])
            user = self.namedtuplefetchall(cursor)
            return user

    def find_by_ens(self, mat):
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT cours.code AS code, cours.descrisption AS description, cours.id_niveau AS niv, cours.id_fil AS fil FROM cours,enseignant WHERE cours.matricule_ensg=enseignant.matricule_ensg AND enseignant.matricule_ensg=%s",[mat])
            user = self.namedtuplefetchall(cursor)
            return user
    def find_file_txt(self,niv,fil,code):
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT cours.code AS cd,fichier.descriptif AS descrip,fichier.contenu AS lien,lier.date_publication AS date_p FROM cours,fichier,type_fichier,lier WHERE cours.code=lier.code AND fichier.id_fichier=lier.id_fichier AND lier.id_type AND cours.id_niveau=%s AND cours.id_fil=%s AND type_fichier.nom_type=%s AND cours.code=%s ORDER BY date_p DESC",[niv,fil,'texte',code])
            user = self.namedtuplefetchall(cursor)
            return user
    def ajouter(self,code,description,niv,fil,ens):
        with connection.cursor() as cursor:
            cursor.execute(
                "INSERT INTO cours(code,descrisption,id_niveau,id_fil,matricule_ensg) VALUES (%s,%s,%s,%s,%s)",
                [code,description,niv,fil,ens])
            return True
    def MAJ_cours(self,code,description,niv,fil,ens):
        with connection.cursor() as cursor:
            cursor.execute(
                "UPDATE cours SET descrisption=%s,id_niveau=%s,id_fil=%s,matricule_ensg=%s WHERE code=%s",
                [description,niv,fil,ens,code])
            return True
    def sup_cours(self,code):
        with connection.cursor() as cursor:
            cursor.execute(
                "DELETE FROM cours WHERE code=%s",
                [code])
            return True



class babibllard:
    def namedtuplefetchall(self, cursor):
        desc = cursor.description
        nt_result = namedtuple('Result', [col[0] for col in desc])
        return [nt_result(*row) for row in cursor.fetchall()]

    def set_notes(self,mat,sem):
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT etudiant.matricule_etd,etudiant.nom,cours.descrisption AS descrip,note.note_cc AS cc ,note.note_tp AS tp,note.note_sn AS sn,sum(note.note_cc+note.note_tp+note.note_sn) AS som from cours, note,etudiant where note.matricule_etd=%s and note.matricule_etd=etudiant.matricule_etd and cours.code=note.code and note.id_sem=%s GROUP BY note.code",[mat,sem])
            user = self.namedtuplefetchall(cursor)
            return user
class amitie:
    def namedtuplefetchall(self, cursor):
        desc = cursor.description
        nt_result = namedtuple('Result', [col[0] for col in desc])
        return [nt_result(*row) for row in cursor.fetchall()]


    def demander(self,etd1,etd2):
        with connection.cursor() as cursor:
            cursor.execute(
                "INSERT INTO ami(etd1,etd2) VALUES(%s,%s)",[etd1,etd2])
            return True
    def status_demande(self,etd1,etd2):

        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT status FROM ami WHERE etd1=%s AND etd2=%s",[etd1,etd2])
            user = self.namedtuplefetchall(cursor)
            print(user)
            print(etd1)
            print(etd2)
            if not user:
                return 3
            else:
                res=user[0].status
                return res
    def accepter(self,etd1,etd2):
        with connection.cursor() as cursor:
            cursor.execute(
                "UPDATE ami set status='1' WHERE etd1=%s and etd2=%s",[etd1,etd2])
            return True
    def refuser(self,etd1,etd2):
        with connection.cursor() as cursor:
            cursor.execute(
                "DELETE FROM ami WHERE etd1=%s and etd2=%s",[etd1,etd2])
            return True

class publication:
    def namedtuplefetchall(self, cursor):
        desc = cursor.description
        nt_result = namedtuple('Result', [col[0] for col in desc])
        return [nt_result(*row) for row in cursor.fetchall()]

    def save_publi_ens(self,titre,descriptif,niv,fil,fichier,mat,code):
        dest = "/media/publication/"
        if fichier.content_type == 'image/jpeg' or 'image/jpg' or 'image/png' or 'video/mp4':
            fname, extension = os.path.splitext(fichier.name)
            print(fichier.content_type)

            with connection.cursor() as cursor:
                cursor.execute(
                    "INSERT INTO fichier(descriptif,contenu) VALUES(%s,%s)", [descriptif, dest + fichier.name])
                last = cursor.lastrowid
                last_id_fichier = last

            if fichier.content_type == 'video/mp4':
                type_fil = 2

            elif fichier.content_type == 'image/jpeg' or 'image/jpg' or 'image/png':
                type_fil = 4

            with connection.cursor() as cursor:
                cursor.execute(
                    "INSERT INTO lier(code,id_fichier,id_type) VALUES(%s,%s,%s)", [code, last, type_fil])

            with connection.cursor() as cursor:
                cursor.execute(
                    "INSERT INTO publication(matricule_ensg,titre,description,id_fichier,id_fil,id_niveau,status) VALUES(%s,%s,%s,%s,$s,%s,%s)",
                    [mat, titre, descriptif, last, fil, niv,'1'])
                last_id_publi = cursor.lastrowid
            fichier.name = str(last_id_publi) + extension
            with connection.cursor() as cursor:
                cursor.execute(
                    "UPDATE fichier SET contenu=%s WHERE id_fichier=%s", [dest + fichier.name, last_id_fichier])
                last = cursor.lastrowid
        return True
    def save_publi_ens_no_file(self, titre, descriptif, niv, fil, mat, code):


        with connection.cursor() as cursor:
            cursor.execute(
                "INSERT INTO publication(matricule_ensg,titre,description,id_fil,id_niveau,code,status) VALUES(%s,%s,%s,%s,%s,%s,%s)",
                [mat, titre, descriptif, fil, niv,code,'1'])

        return True

    def save_publi_etd_no_file(self, titre, descriptif, niv, fil, mat, code):


        with connection.cursor() as cursor:
            cursor.execute(
                "INSERT INTO publication(matricule_etd,titre,description,id_fil,id_niveau,code) VALUES(%s,%s,%s,%s,%s,%s)",
                [mat, titre, descriptif, fil, niv,code])

        return True

    def save_publi_etd(self, titre, descriptif, niv, fil, fichier, mat, code):
        dest = "/media/publication/"
        if fichier.content_type == 'image/jpeg' or 'image/jpg' or 'image/png' or 'video/mp4':
            fname, extension = os.path.splitext(fichier.name)
            print(fichier.content_type)

            with connection.cursor() as cursor:
                cursor.execute(
                    "INSERT INTO fichier(descriptif,contenu) VALUES(%s,%s)", [descriptif, dest + fichier.name])
                last = cursor.lastrowid
                last_id_fichier=last

            if  fichier.content_type == 'video/mp4':
                type_fil = 2

            elif fichier.content_type == 'image/jpeg' or 'image/jpg' or 'image/png':
                type_fil = 4

            with connection.cursor() as cursor:
                cursor.execute(
                    "INSERT INTO lier(code,id_fichier,id_type) VALUES(%s,%s,%s)", [code, last, type_fil])

            with connection.cursor() as cursor:
                cursor.execute(
                    "INSERT INTO publication(matricule_etd,titre,description,id_fichier,id_fil,id_niveau) VALUES(%s,%s,%s,%s,%s,%s)",
                    [mat, titre, descriptif, last, fil, niv])
                last_id_publi = cursor.lastrowid
            fichier.name=str(last_id_publi)+extension
            with connection.cursor() as cursor:
                cursor.execute(
                    "UPDATE fichier SET contenu=%s WHERE id_fichier=%s", [dest + fichier.name,last_id_fichier])
                last = cursor.lastrowid
        return True
    def like(self,publi,mat):
        with connection.cursor() as cursor:
            cursor.execute(
                "INSERT INTO liked(id_publication,matricule_etd) VALUES(%s,%s)", [publi, mat])
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT liked FROM publication WHERE id_publication=%s", [publi])
            user=self.namedtuplefetchall(cursor)
            count=user[0].liked
        with connection.cursor() as cursor:
            cursor.execute(
                "UPDATE publication SET liked=%s WHERE id_publication=%s", [count+1,publi])
        return True
    def del_like(self,publi,mat):
        with connection.cursor() as cursor:
            cursor.execute(
                "DELETE FROM liked WHERE id_publication=%s AND matricule_etd=%s", [publi, mat])
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT liked FROM publication WHERE id_publication=%s", [publi])
            user=self.namedtuplefetchall(cursor)
            count=user[0].liked
        with connection.cursor() as cursor:
            cursor.execute(
                "UPDATE publication SET liked=%s WHERE id_publication=%s", [count-1,publi])
        return True

    def is_like_ens(self,publi,mat):
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT id,id_publication FROM liked WHERE id_publication=%s AND matricule_ensg=%s", [publi, mat])
            rs=self.namedtuplefetchall(cursor)
        if rs:
            return 1
        else:
            return 0

    def is_like_etd(self,publi,mat):
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT id,id_publication FROM liked WHERE id_publication=%s AND matricule_etd=%s", [publi, mat])
            rs=self.namedtuplefetchall(cursor)
        return rs



    def total_like(self,publi):
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT id_publication,liked FROM publication WHERE id_publication=%s", [publi])
            total = self.namedtuplefetchall(cursor)
        return total[0]

    def total_unlike(self,publi):
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT id_publication,unlike FROM publication WHERE id_publication=%s", [publi])
            total = self.namedtuplefetchall(cursor)
        return total[0]

    def commenter_etd(self,publi,mat,comment):
        with connection.cursor() as cursor:
            cursor.execute(
                "INSERT INTO commentaire(id_publication,matricule_etd,contenu) VALUES(%s,%s,%s)", [publi, mat, comment])
        return True

    def commenter_ens(self,publi,mat,comment):
        with connection.cursor() as cursor:
            cursor.execute(
                "INSERT INTO commentaire(id_publication,matricule_ensg,contenu) VALUES(%s,%s,%s)", [publi, mat, comment])
        return True

    def all_comment(self,publi):
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT id_publication,matricule_etd,matricule_ensg,contenu FROM commentaire WHERE id_publication=%s", [publi])
            total = self.namedtuplefetchall(cursor)
        return total
    def nb_comment(self,publi):
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT COUNT(id_com) AS nb_com,id_publication FROM commentaire WHERE id_publication=%s", [publi])
            total = self.namedtuplefetchall(cursor)
        return total
    def acccept_publi(self,publi,mat):
        with connection.cursor() as cursor:
            cursor.execute(
                "UPDATE publication SET status='1' WHERE id_publication=%s AND matricule_etd=%s", [publi,mat])
        return True
    def not_acccept_publi(self,publi,mat):
        with connection.cursor() as cursor:
            cursor.execute(
                "UPDATE publication SET status='2' WHERE id_publication=%s AND matricule_etd=%s", [publi,mat])
        return True

    def all_publi_etd(self,mat):
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT publication.date_pub AS date,publication.id_publication AS id_publi,publication.titre AS titre, publication.description AS description, fichier.contenu AS contenu, type_fichier.nom_type AS type FROM publication,fichier,type_fichier,lier WHERE publication.matricule_etd=%s AND publication.status='1' AND publication.id_fichier=lier.id_fichier AND publication.id_fichier=fichier.id_fichier AND lier.id_type=type_fichier.id_type", [mat])
            total = self.namedtuplefetchall(cursor)
        return total
    def all_publi_no_file_etd(self,mat):
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT publication.date_pub AS date,publication.id_publication AS id_publi,publication.titre AS titre, publication.description AS description FROM publication WHERE publication.matricule_etd=%s AND publication.status='1' AND publication.id_fichier IS NULL ", [mat])
            total = self.namedtuplefetchall(cursor)
        return total

    def all_publi_ens(self,mat):
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT publication.id_publication AS id_publi,publication.titre AS titre, publication.description AS description, fichier.contenu AS contenu, type_fichier.nom_type AS type FROM publication,fichier,type_fichier,lier WHERE publication.matricule_ensg=%s AND publication.status='1' AND publication.id_fichier=lier.id_fichier AND publication.id_fichier=fichier.id_fichier AND lier.id_type=type_fichier.id_type", [mat])
            total = self.namedtuplefetchall(cursor)
        return total
    def all_publi_no_file_ens(self,mat):
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT publication.date_pub AS date,publication.id_publication AS id_publi,publication.titre AS titre, publication.description AS description FROM publication WHERE publication.matricule_ensg=%s AND publication.status='1' AND publication.id_fichier IS NULL ", [mat])
            total = self.namedtuplefetchall(cursor)
        return total

    def fil_actu_etd(self,niv,fil):
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT publication.id_publication AS id_publi,publication.date_pub AS date,publication.matricule_etd AS etd,publication.matricule_ensg AS ens,publication.titre AS titre, publication.description AS description, fichier.contenu AS contenu, type_fichier.nom_type AS type FROM publication,fichier,type_fichier,lier WHERE publication.id_fil=%s AND publication.id_niveau=%s AND  publication.status='1' AND publication.id_fichier=lier.id_fichier AND publication.id_fichier=fichier.id_fichier AND lier.id_type=type_fichier.id_type ORDER BY publication.date_pub DESC", [fil,niv])
            total = self.namedtuplefetchall(cursor)
        return total
    def fil_actu_no_file_etd(self,niv,fil):
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT publication.matricule_etd AS etd,publication.matricule_ensg AS ens,publication.date_pub AS date,publication.id_publication AS id_publi,publication.titre AS titre, publication.description AS description FROM publication WHERE publication.id_niveau=%s AND publication.id_fil=%s AND  publication.status='1' AND publication.id_fichier IS NULL ORDER BY publication.date_pub DESC ", [niv,fil])
            total = self.namedtuplefetchall(cursor)
        return total

    def fil_actu_ens(self,fil):
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT publication.id_publication AS id_publi,publication.date_pub AS date,publication.matricule_etd AS etd,publication.matricule_ensg AS ens,publication.titre AS titre, publication.description AS description, fichier.contenu AS contenu, type_fichier.nom_type AS type FROM publication,fichier,type_fichier,lier WHERE  publication.status='1' AND publication.id_fil=%s  AND publication.id_fichier=lier.id_fichier AND publication.id_fichier=fichier.id_fichier AND lier.id_type=type_fichier.id_type ORDER BY publication.date_pub DESC",
                [fil])
            total = self.namedtuplefetchall(cursor)
        return total
    def fil_actu_no_file_ens(self,fil):
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT publication.matricule_etd AS etd,publication.matricule_ensg AS ens,publication.date_pub AS date,publication.id_publication AS id_publi,publication.titre AS titre, publication.description AS description FROM publication WHERE publication.id_fil=%s AND publication.id_fichier IS NULL ORDER BY publication.date_pub DESC ",
                [fil])
            total = self.namedtuplefetchall(cursor)
        return total

    def fil_actu_ad(self,niv,fil):
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT publication.id_publication AS id_publi,publication.date_pub AS date,publication.matricule_etd AS etd,publication.matricule_ensg AS ens,publication.titre AS titre, publication.description AS description, fichier.contenu AS contenu, type_fichier.nom_type AS type FROM publication,fichier,type_fichier,lier WHERE publication.matricule_ensg IS NULL AND publication.status='0' AND publication.id_fil=%s AND publication.id_niveau=%s AND publication.id_fichier=lier.id_fichier AND publication.id_fichier=fichier.id_fichier AND lier.id_type=type_fichier.id_type ORDER BY publication.date_pub DESC",
                [fil, niv])
            total = self.namedtuplefetchall(cursor)
        return total
    def fil_actu_no_file_ad(self,niv,fil):
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT publication.matricule_etd AS etd,publication.matricule_ensg AS ens,publication.date_pub AS date,publication.id_publication AS id_publi,publication.titre AS titre, publication.description AS description FROM publication WHERE publication.matricule_ensg IS NULL AND publication.id_niveau=%s AND publication.id_fil=%s AND publication.id_fichier IS NULL AND publication.status='0' ORDER BY publication.date_pub DESC",[niv,fil])
            total = self.namedtuplefetchall(cursor)
        return total


