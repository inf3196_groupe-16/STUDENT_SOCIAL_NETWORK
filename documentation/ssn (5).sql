-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mar. 21 juin 2022 à 13:06
-- Version du serveur :  8.0.21
-- Version de PHP : 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `ssn`
--

-- --------------------------------------------------------

--
-- Structure de la table `acceder`
--

DROP TABLE IF EXISTS `acceder`;
CREATE TABLE IF NOT EXISTS `acceder` (
  `matricule_etd` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `code` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`code`,`matricule_etd`),
  KEY `matricule_etd` (`matricule_etd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `matricule_ad` varchar(10) NOT NULL,
  `password` varchar(25) NOT NULL,
  `id_fil` varchar(10) NOT NULL,
  `id_niveau` int NOT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(25) NOT NULL,
  `prenom` varchar(25) NOT NULL,
  PRIMARY KEY (`matricule_ad`),
  UNIQUE KEY `id` (`id`),
  KEY `id_fil` (`id_fil`),
  KEY `id_niveau` (`id_niveau`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `admin`
--

INSERT INTO `admin` (`matricule_ad`, `password`, `id_fil`, `id_niveau`, `id`, `nom`, `prenom`) VALUES
('ad-inf-01', 'qwerty123', 'INF', 3, 1, 'mbonga', 'Evelyne');

-- --------------------------------------------------------

--
-- Structure de la table `adresse`
--

DROP TABLE IF EXISTS `adresse`;
CREATE TABLE IF NOT EXISTS `adresse` (
  `id_adresse` int NOT NULL,
  `nom_adresse` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id_adresse`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `adresse`
--

INSERT INTO `adresse` (`id_adresse`, `nom_adresse`) VALUES
(0, NULL),
(1, 'Omnisport'),
(2, 'Damas');

-- --------------------------------------------------------

--
-- Structure de la table `ami`
--

DROP TABLE IF EXISTS `ami`;
CREATE TABLE IF NOT EXISTS `ami` (
  `etd1` varchar(9) COLLATE utf8mb4_general_ci NOT NULL,
  `etd2` varchar(9) COLLATE utf8mb4_general_ci NOT NULL,
  `status` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`etd1`,`etd2`),
  KEY `etd2` (`etd2`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `ami`
--

INSERT INTO `ami` (`etd1`, `etd2`, `status`, `created_at`) VALUES
('19Y085', '19Y460', '0', '2022-06-21 09:41:04');

-- --------------------------------------------------------

--
-- Structure de la table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
CREATE TABLE IF NOT EXISTS `auth_group` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
CREATE TABLE IF NOT EXISTS `auth_group_permissions` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `group_id` int NOT NULL,
  `permission_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_group_id_b120cbf9` (`group_id`),
  KEY `auth_group_permissions_permission_id_84c5c92e` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
CREATE TABLE IF NOT EXISTS `auth_permission` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  KEY `auth_permission_content_type_id_2f476e4b` (`content_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can view log entry', 1, 'view_logentry'),
(5, 'Can add permission', 2, 'add_permission'),
(6, 'Can change permission', 2, 'change_permission'),
(7, 'Can delete permission', 2, 'delete_permission'),
(8, 'Can view permission', 2, 'view_permission'),
(9, 'Can add group', 3, 'add_group'),
(10, 'Can change group', 3, 'change_group'),
(11, 'Can delete group', 3, 'delete_group'),
(12, 'Can view group', 3, 'view_group'),
(13, 'Can add user', 4, 'add_user'),
(14, 'Can change user', 4, 'change_user'),
(15, 'Can delete user', 4, 'delete_user'),
(16, 'Can view user', 4, 'view_user'),
(17, 'Can add content type', 5, 'add_contenttype'),
(18, 'Can change content type', 5, 'change_contenttype'),
(19, 'Can delete content type', 5, 'delete_contenttype'),
(20, 'Can view content type', 5, 'view_contenttype'),
(21, 'Can add session', 6, 'add_session'),
(22, 'Can change session', 6, 'change_session'),
(23, 'Can delete session', 6, 'delete_session'),
(24, 'Can view session', 6, 'view_session'),
(25, 'Can add adresse', 7, 'add_adresse'),
(26, 'Can change adresse', 7, 'change_adresse'),
(27, 'Can delete adresse', 7, 'delete_adresse'),
(28, 'Can view adresse', 7, 'view_adresse'),
(29, 'Can add cours', 8, 'add_cours'),
(30, 'Can change cours', 8, 'change_cours'),
(31, 'Can delete cours', 8, 'delete_cours'),
(32, 'Can view cours', 8, 'view_cours'),
(33, 'Can add enseignant', 9, 'add_enseignant'),
(34, 'Can change enseignant', 9, 'change_enseignant'),
(35, 'Can delete enseignant', 9, 'delete_enseignant'),
(36, 'Can view enseignant', 9, 'view_enseignant'),
(37, 'Can add etudiant', 10, 'add_etudiant'),
(38, 'Can change etudiant', 10, 'change_etudiant'),
(39, 'Can delete etudiant', 10, 'delete_etudiant'),
(40, 'Can view etudiant', 10, 'view_etudiant'),
(41, 'Can add fichier', 11, 'add_fichier'),
(42, 'Can change fichier', 11, 'change_fichier'),
(43, 'Can delete fichier', 11, 'delete_fichier'),
(44, 'Can view fichier', 11, 'view_fichier'),
(45, 'Can add filiere', 12, 'add_filiere'),
(46, 'Can change filiere', 12, 'change_filiere'),
(47, 'Can delete filiere', 12, 'delete_filiere'),
(48, 'Can view filiere', 12, 'view_filiere'),
(49, 'Can add niveau', 13, 'add_niveau'),
(50, 'Can change niveau', 13, 'change_niveau'),
(51, 'Can delete niveau', 13, 'delete_niveau'),
(52, 'Can view niveau', 13, 'view_niveau'),
(53, 'Can add semestre', 14, 'add_semestre'),
(54, 'Can change semestre', 14, 'change_semestre'),
(55, 'Can delete semestre', 14, 'delete_semestre'),
(56, 'Can view semestre', 14, 'view_semestre'),
(57, 'Can add type fichier', 15, 'add_typefichier'),
(58, 'Can change type fichier', 15, 'change_typefichier'),
(59, 'Can delete type fichier', 15, 'delete_typefichier'),
(60, 'Can view type fichier', 15, 'view_typefichier'),
(61, 'Can add publication', 16, 'add_publication'),
(62, 'Can change publication', 16, 'change_publication'),
(63, 'Can delete publication', 16, 'delete_publication'),
(64, 'Can view publication', 16, 'view_publication'),
(65, 'Can add note', 17, 'add_note'),
(66, 'Can change note', 17, 'change_note'),
(67, 'Can delete note', 17, 'delete_note'),
(68, 'Can view note', 17, 'view_note'),
(69, 'Can add message', 18, 'add_message'),
(70, 'Can change message', 18, 'change_message'),
(71, 'Can delete message', 18, 'delete_message'),
(72, 'Can view message', 18, 'view_message'),
(73, 'Can add commentaire', 19, 'add_commentaire'),
(74, 'Can change commentaire', 19, 'change_commentaire'),
(75, 'Can delete commentaire', 19, 'delete_commentaire'),
(76, 'Can view commentaire', 19, 'view_commentaire'),
(77, 'Can add lier', 20, 'add_lier'),
(78, 'Can change lier', 20, 'change_lier'),
(79, 'Can delete lier', 20, 'delete_lier'),
(80, 'Can view lier', 20, 'view_lier'),
(81, 'Can add acceder', 21, 'add_acceder'),
(82, 'Can change acceder', 21, 'change_acceder'),
(83, 'Can delete acceder', 21, 'delete_acceder'),
(84, 'Can view acceder', 21, 'view_acceder'),
(85, 'Can add image', 22, 'add_image'),
(86, 'Can change image', 22, 'change_image'),
(87, 'Can delete image', 22, 'delete_image'),
(88, 'Can view image', 22, 'view_image'),
(89, 'Can add image_ens', 23, 'add_image_ens'),
(90, 'Can change image_ens', 23, 'change_image_ens'),
(91, 'Can delete image_ens', 23, 'delete_image_ens'),
(92, 'Can view image_ens', 23, 'view_image_ens'),
(93, 'Can add cours_doc', 24, 'add_cours_doc'),
(94, 'Can change cours_doc', 24, 'change_cours_doc'),
(95, 'Can delete cours_doc', 24, 'delete_cours_doc'),
(96, 'Can view cours_doc', 24, 'view_cours_doc'),
(97, 'Can add file_public', 25, 'add_file_public'),
(98, 'Can change file_public', 25, 'change_file_public'),
(99, 'Can delete file_public', 25, 'delete_file_public'),
(100, 'Can view file_public', 25, 'view_file_public'),
(101, 'Can add chat message', 26, 'add_chatmessage'),
(102, 'Can change chat message', 26, 'change_chatmessage'),
(103, 'Can delete chat message', 26, 'delete_chatmessage'),
(104, 'Can view chat message', 26, 'view_chatmessage'),
(105, 'Can add thread', 27, 'add_thread'),
(106, 'Can change thread', 27, 'change_thread'),
(107, 'Can delete thread', 27, 'delete_thread'),
(108, 'Can view thread', 27, 'view_thread');

-- --------------------------------------------------------

--
-- Structure de la table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
CREATE TABLE IF NOT EXISTS `auth_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$320000$8BNrWtalZQuN3lj6CscHb0$NhP85yl6aPvbIUSReqALEFIEf39wIe6C2o8oe1btkEc=', NULL, 1, 'hp', '', '', 'nzadouo@gmail.com', 1, 1, '2022-06-20 18:46:00.809271');

-- --------------------------------------------------------

--
-- Structure de la table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
CREATE TABLE IF NOT EXISTS `auth_user_groups` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `group_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_user_id_6a12ed8b` (`user_id`),
  KEY `auth_user_groups_group_id_97559544` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
CREATE TABLE IF NOT EXISTS `auth_user_user_permissions` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `permission_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_user_id_a95ead1b` (`user_id`),
  KEY `auth_user_user_permissions_permission_id_1fbb5f2c` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

DROP TABLE IF EXISTS `commentaire`;
CREATE TABLE IF NOT EXISTS `commentaire` (
  `id_com` int NOT NULL AUTO_INCREMENT,
  `matricule_ensg` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `matricule_etd` varchar(9) DEFAULT NULL,
  `id_publication` int DEFAULT NULL,
  `date_comment` datetime DEFAULT CURRENT_TIMESTAMP,
  `contenu` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_com`),
  KEY `id_publication` (`id_publication`),
  KEY `matricule_ensg` (`matricule_ensg`),
  KEY `matricule_etd` (`matricule_etd`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `commentaire`
--

INSERT INTO `commentaire` (`id_com`, `matricule_ensg`, `matricule_etd`, `id_publication`, `date_comment`, `contenu`) VALUES
(37, '04-inf-15', NULL, 31, '2022-06-06 16:30:44', 'kiii'),
(39, '04-inf-15', NULL, 31, '2022-06-06 16:36:59', 'ok'),
(40, NULL, '19Y085', 31, '2022-06-06 16:37:22', 'ok'),
(41, '04-inf-15', NULL, 35, '2022-06-15 11:28:11', 'com 1');

-- --------------------------------------------------------

--
-- Structure de la table `cours`
--

DROP TABLE IF EXISTS `cours`;
CREATE TABLE IF NOT EXISTS `cours` (
  `code` varchar(10) NOT NULL,
  `descrisption` varchar(50) DEFAULT NULL,
  `date_diff` date DEFAULT NULL,
  `id_niveau` int DEFAULT NULL,
  `id_fil` varchar(10) DEFAULT NULL,
  `matricule_ensg` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`code`),
  KEY `id_fil` (`id_fil`),
  KEY `id_niveau` (`id_niveau`),
  KEY `matricule_ensg` (`matricule_ensg`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `cours`
--

INSERT INTO `cours` (`code`, `descrisption`, `date_diff`, `id_niveau`, `id_fil`, `matricule_ensg`) VALUES
('INF3015', 'algorithme', NULL, 3, 'INF', '04-inf-15'),
('INF3036', 'base de donnes', '2022-04-21', 3, 'INF', '04-inf-15'),
('INF3046', 'systeme d\'exploitation', '2022-05-10', 3, 'INF', '04-inf-15');

-- --------------------------------------------------------

--
-- Structure de la table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
CREATE TABLE IF NOT EXISTS `django_admin_log` (
  `id` int NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint UNSIGNED NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int DEFAULT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6` (`user_id`)
) ;

-- --------------------------------------------------------

--
-- Structure de la table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
CREATE TABLE IF NOT EXISTS `django_content_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(3, 'auth', 'group'),
(2, 'auth', 'permission'),
(4, 'auth', 'user'),
(5, 'contenttypes', 'contenttype'),
(6, 'sessions', 'session'),
(21, 'ssn_app', 'acceder'),
(7, 'ssn_app', 'adresse'),
(26, 'ssn_app', 'chatmessage'),
(19, 'ssn_app', 'commentaire'),
(8, 'ssn_app', 'cours'),
(24, 'ssn_app', 'cours_doc'),
(9, 'ssn_app', 'enseignant'),
(10, 'ssn_app', 'etudiant'),
(11, 'ssn_app', 'fichier'),
(25, 'ssn_app', 'file_public'),
(12, 'ssn_app', 'filiere'),
(22, 'ssn_app', 'image'),
(23, 'ssn_app', 'image_ens'),
(20, 'ssn_app', 'lier'),
(18, 'ssn_app', 'message'),
(13, 'ssn_app', 'niveau'),
(17, 'ssn_app', 'note'),
(16, 'ssn_app', 'publication'),
(14, 'ssn_app', 'semestre'),
(27, 'ssn_app', 'thread'),
(15, 'ssn_app', 'typefichier');

-- --------------------------------------------------------

--
-- Structure de la table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
CREATE TABLE IF NOT EXISTS `django_migrations` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2022-05-23 09:18:05.060856'),
(2, 'auth', '0001_initial', '2022-05-23 09:18:13.729011'),
(3, 'admin', '0001_initial', '2022-05-23 09:18:15.492523'),
(4, 'admin', '0002_logentry_remove_auto_add', '2022-05-23 09:18:15.500471'),
(5, 'admin', '0003_logentry_add_action_flag_choices', '2022-05-23 09:18:15.508053'),
(6, 'contenttypes', '0002_remove_content_type_name', '2022-05-23 09:18:16.098395'),
(7, 'auth', '0002_alter_permission_name_max_length', '2022-05-23 09:18:16.374621'),
(8, 'auth', '0003_alter_user_email_max_length', '2022-05-23 09:18:16.956859'),
(9, 'auth', '0004_alter_user_username_opts', '2022-05-23 09:18:16.964835'),
(10, 'auth', '0005_alter_user_last_login_null', '2022-05-23 09:18:17.654223'),
(11, 'auth', '0006_require_contenttypes_0002', '2022-05-23 09:18:17.657390'),
(12, 'auth', '0007_alter_validators_add_error_messages', '2022-05-23 09:18:17.721530'),
(13, 'auth', '0008_alter_user_username_max_length', '2022-05-23 09:18:17.960965'),
(14, 'auth', '0009_alter_user_last_name_max_length', '2022-05-23 09:18:18.339687'),
(15, 'auth', '0010_alter_group_name_max_length', '2022-05-23 09:18:18.613813'),
(16, 'auth', '0011_update_proxy_permissions', '2022-05-23 09:18:18.625782'),
(17, 'auth', '0012_alter_user_first_name_max_length', '2022-05-23 09:18:18.901314'),
(18, 'sessions', '0001_initial', '2022-05-23 09:18:19.271653'),
(19, 'ssn_app', '0001_initial', '2022-05-30 08:01:00.213288'),
(20, 'ssn_app', '0002_etudiant_pp', '2022-05-30 08:01:00.570368'),
(21, 'ssn_app', '0003_alter_etudiant_pp', '2022-05-30 08:01:00.677093'),
(22, 'ssn_app', '0004_image_remove_etudiant_pp', '2022-05-30 08:01:00.715467'),
(23, 'ssn_app', '0005_rename_pp_image_photo_profil', '2022-05-30 08:01:00.779080'),
(24, 'ssn_app', '0006_remove_image_photo_profil_image_img', '2022-05-30 08:01:00.857241'),
(25, 'ssn_app', '0007_alter_image_table', '2022-05-30 08:29:11.357079'),
(26, 'ssn_app', '0008_image_ens_alter_image_img', '2022-05-30 13:49:01.770813'),
(27, 'ssn_app', '0009_cours_doc', '2022-05-30 19:41:56.585687'),
(28, 'ssn_app', '0010_remove_cours_doc_description', '2022-06-02 01:04:54.716441'),
(29, 'ssn_app', '0011_file_public', '2022-06-02 01:04:55.020197'),
(30, 'ssn_app', '0012_alter_file_public_publication', '2022-06-05 22:07:38.138482'),
(31, 'ssn_app', '0013_alter_file_public_publication', '2022-06-05 22:19:30.259169'),
(32, 'ssn_app', '0014_thread_chatmessage', '2022-06-20 19:02:28.044976');

-- --------------------------------------------------------

--
-- Structure de la table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
CREATE TABLE IF NOT EXISTS `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('7ohugg27sv4qhejt565tmxon36ofazig', 'eyJ1c2VyX2lkIjo3LCJVc2VybmFtZSI6IjE5WTA4NSIsIm5pdmVhdSI6MywiZmlsaWVyZSI6IklORiIsImZvbmN0aW9uIjoiZXR1ZGlhbnQifQ:1o30Rv:4-niFZXXYSIj_jKAnkwI6K06Jm--x1UZpIS7LTtgd0Y', '2022-07-03 19:19:07.346027'),
('8nbx7qkcwcdqk2hfixps0gw7k79rf65j', 'eyJ1c2VyX2lkIjo3LCJVc2VybmFtZSI6IjE5WTA4NSIsIm5pdmVhdSI6MywiZmlsaWVyZSI6IklORiIsImZvbmN0aW9uIjoiZXR1ZGlhbnQifQ:1o3Lvi:4cA2QNq7j2gsU2tXOpQ8QN6077dvg2G61e18zAQsQM4', '2022-07-04 18:15:18.164061'),
('91mvvgs92vh1ojo0duaku57okg9dn9xt', 'eyJ1c2VyX2lkIjo3LCJVc2VybmFtZSI6IjE5WTA4NSIsIm5pdmVhdSI6MywiZmlsaWVyZSI6IklORiIsImZvbmN0aW9uIjoiZXR1ZGlhbnQifQ:1o1PkL:XxW0JpYdRwHUxO4qdaHZug70AqF-xg3b7wfuq9OWl7A', '2022-06-29 09:55:33.719619'),
('9o7cvii30i7wsw2lf482s5k4u0zvdekb', 'eyJ1c2VyX2lkIjo3LCJVc2VybmFtZSI6IjE5WTA4NSIsIm5pdmVhdSI6MywiZmlsaWVyZSI6IklORiIsImZvbmN0aW9uIjoiZXR1ZGlhbnQifQ:1nyWEq:GUKzUUmO_nYJByhTSaIusUM5E5KVFyqQXgJza5OXXg8', '2022-06-21 10:15:04.718443'),
('apjb2da7qwdotd1igdt79dub54g6p6ki', 'eyJ1c2VyX2lkIjo3LCJVc2VybmFtZSI6IjE5WTA4NSIsIm5pdmVhdSI6MywiZmlsaWVyZSI6IklORiIsImZvbmN0aW9uIjoiZXR1ZGlhbnQifQ:1ny2Sd:Wv2dlPaOCYQdix0D9RK2R0ZRggQz-c_gFWbPIWOmWRo', '2022-06-20 02:27:19.875577'),
('c8exbpx7n3janovs8b1cheojjea6v3i5', 'eyJ1c2VyX2lkIjo3LCJVc2VybmFtZSI6IjE5WTA4NSIsIm5pdmVhdSI6MywiZmlsaWVyZSI6IklORiIsImZvbmN0aW9uIjoiZXR1ZGlhbnQifQ:1nxEGg:uAXRmfzpb_F1wJs2xEciPR8HgzlWVMnAFMpEAV2cr_8', '2022-06-17 20:51:38.500238'),
('exryt0mmg89eh7j657qu4mxtopihqhgf', 'eyJ1c2VyX2lkIjoxLCJVc2VybmFtZSI6ImFkLWluZi0wMSIsImZpbGllcmUiOiJJTkYiLCJuaXZlYXUiOjMsImZvbmN0aW9uIjoiYWRtaW4ifQ:1nyYyD:n3_4MV11qMafKMbE0Ww7Q-miYFdmOpZu1ODLXF5t05Q', '2022-06-21 13:10:05.371135'),
('fdzeg7wjtbd13hpfcpjy83difa4q36p1', 'eyJ1c2VyX2lkIjoyLCJVc2VybmFtZSI6IjA0LWluZi0xNSIsImZpbGllcmUiOiJJTkYiLCJmb25jdGlvbiI6ImVuc2VpZ25hbnQifQ:1nyAdz:9hSvJLIqzsJDOJzKspU9BM_tzlwcMNS_EvNNOAV1hrM', '2022-06-20 11:11:35.334178'),
('fj9242v3opm9dwcuox7y67i77lvz04qm', 'eyJ1c2VyX2lkIjoxLCJVc2VybmFtZSI6ImFkLWluZi0wMSIsImZpbGllcmUiOiJJTkYiLCJuaXZlYXUiOjMsImZvbmN0aW9uIjoiYWRtaW4ifQ:1o1QGw:5WejHBmVjHPWYLPX-fuKX1QLVYIsDvMPwYbn_ummAJE', '2022-06-29 10:29:14.372156'),
('i7pl9gjr9ryabgh5cyg4nj8266ko4mdr', 'eyJ1c2VyX2lkIjoxLCJVc2VybmFtZSI6ImFkLWluZi0wMSIsImZpbGllcmUiOiJJTkYiLCJuaXZlYXUiOjMsImZvbmN0aW9uIjoiYWRtaW4ifQ:1o12vP:xsBHey-_pPwUeuUMP2yWNwMQROsG-vdOT2PSPjn_NPc', '2022-06-28 09:33:27.107146'),
('i9jxco0p74rtdjg8ibh617jzafrllb0u', 'eyJ1c2VyX2lkIjoyLCJVc2VybmFtZSI6IjA0LWluZi0xNSIsImZpbGllcmUiOiJJTkYiLCJmb25jdGlvbiI6ImVuc2VpZ25hbnQifQ:1nyWEK:0XsMRpUSGg41_LSGf_fIAPjLF73iyZOXZS7eQ5RoFQI', '2022-06-21 10:14:32.753337'),
('j6qit42bky0k5o2obvd7a7o25j750218', 'eyJ1c2VyX2lkIjo3LCJVc2VybmFtZSI6IjE5WTA4NSIsIm5pdmVhdSI6MywiZmlsaWVyZSI6IklORiIsImZvbmN0aW9uIjoiZXR1ZGlhbnQifQ:1nx9nw:BeeoV58kGy-95_9EIiziVftGW50dJVGr0DcguMHuAio', '2022-06-17 16:05:40.210435'),
('kef4fbmids0eefyb0skddtmfxojdjenq', 'eyJ1c2VyX2lkIjo3LCJVc2VybmFtZSI6IjE5WTA4NSIsIm5pdmVhdSI6MywiZmlsaWVyZSI6IklORiIsImZvbmN0aW9uIjoiZXR1ZGlhbnQifQ:1nwRYL:i26m9e1AzMLC5snS14EYwlwSYe7NFUwTAA-UmXt6n_k', '2022-06-15 16:50:37.923962'),
('o1f1mb0vwrnn3ninfd80q17inkdi9uhc', 'eyJ1c2VyX2lkIjoyLCJVc2VybmFtZSI6IjA0LWluZi0xNSIsImZpbGllcmUiOiJNQVRIIiwiZm9uY3Rpb24iOiJlbnNlaWduYW50In0:1nvzvD:--ReqRG5lb7EZu16I-iQWHZsE9Yh-AgSiy0a9Stzkt4', '2022-06-14 11:20:23.626969'),
('q0884ufk2sky136qan41mwv9u08b8e48', 'eyJ1c2VyX2lkIjo3LCJVc2VybmFtZSI6IjE5WTA4NSIsIm5pdmVhdSI6MywiZmlsaWVyZSI6IklORiIsImZvbmN0aW9uIjoiZXR1ZGlhbnQifQ:1nwoLq:MHpsvSiKcmmUYAM3z4aFZCvmFnO7kah9kM6JsNpH4cw', '2022-06-16 17:11:14.028802'),
('qzrs551es8w7w096ea5hvkf4zop3f9ud', 'eyJ1c2VyX2lkIjo3LCJVc2VybmFtZSI6IjE5WTA4NSIsIm5pdmVhdSI6MywiZmlsaWVyZSI6IklORiIsImZvbmN0aW9uIjoiZXR1ZGlhbnQifQ:1nx9k6:UZXc9BfJj68x6NFI1X_G5fDnp-eMQh6v4WweJHNL5pQ', '2022-06-17 16:01:42.535536'),
('rw2dh4x2vhukqlxnzas5o598djhn77m8', 'eyJ1c2VyX2lkIjo3LCJVc2VybmFtZSI6IjE5WTA4NSIsIm5pdmVhdSI6MywiZmlsaWVyZSI6IklORiIsImZvbmN0aW9uIjoiZXR1ZGlhbnQifQ:1nw0TG:49zJ_k51QkM1jHMR7lbOgFERe2uNr0ZZaunSymXql8Q', '2022-06-14 11:55:34.233135'),
('scaay9s85f7qx6huk51fhx4poq4f1ri5', 'eyJ1c2VyX2lkIjoyLCJVc2VybmFtZSI6IjA0LWluZi0xNSIsImZpbGllcmUiOiJNQVRIIiwiZm9uY3Rpb24iOiJlbnNlaWduYW50In0:1nvzwY:G6ASV2OZZXFqymLkqUsJyJMiEA9drVYdw7Y1kfs9HYs', '2022-06-14 11:21:46.547988'),
('waeolxrc7x5tc9wefjphbmoai5ug8az6', 'e30:1o3Lsj:GkHBfkFaDntv0JDU0-0Yc-SDXF8nGxGv_7L605Aa69w', '2022-07-04 18:12:13.574159'),
('yd5j90tfn02mzqsraec3a027ww6t6fed', 'eyJ1c2VyX2lkIjo3LCJVc2VybmFtZSI6IjE5WTA4NSIsIm5pdmVhdSI6MywiZmlsaWVyZSI6IklORiIsImZvbmN0aW9uIjoiZXR1ZGlhbnQifQ:1nwS9s:gXXGF13PfkbAhEoZ5Ydll_pU5BK0M6VFZ7e2lpMeLfs', '2022-06-15 17:29:24.908310'),
('yxsmtzt9lc4ju75c44lj1wolpvb9o4ep', 'eyJ1c2VyX2lkIjo3LCJVc2VybmFtZSI6IjE5WTA4NSIsIm5pdmVhdSI6MywiZmlsaWVyZSI6IklORiIsImZvbmN0aW9uIjoiZXR1ZGlhbnQifQ:1nwM9c:DEiXlw1WCSMYdduMq852GywKn31cf2o5Psfs5c-Q_8Y', '2022-06-15 11:04:44.106874'),
('z0y98ickyrwzf9lrwsrla1jieryjgxgx', 'eyJ1c2VyX2lkIjo3LCJVc2VybmFtZSI6IjE5WTA4NSIsIm5pdmVhdSI6MywiZmlsaWVyZSI6IklORiIsImZvbmN0aW9uIjoiZXR1ZGlhbnQifQ:1nwo0q:ugTyRL9dom_4khLCTTh6aWvchiKCeSXPL5b_c9gf0gk', '2022-06-16 16:49:32.326496');

-- --------------------------------------------------------

--
-- Structure de la table `enseignant`
--

DROP TABLE IF EXISTS `enseignant`;
CREATE TABLE IF NOT EXISTS `enseignant` (
  `matricule_ensg` varchar(10) NOT NULL,
  `email` varchar(25) NOT NULL,
  `nom` varchar(25) NOT NULL,
  `PASSWORD` varchar(50) NOT NULL,
  `prenom` varchar(25) DEFAULT NULL,
  `date_nais` date DEFAULT NULL,
  `id_fil` varchar(10) DEFAULT NULL,
  `id_adresse` int DEFAULT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  `photo_profil` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '/media/photo_profil/default.jpg',
  PRIMARY KEY (`matricule_ensg`,`email`),
  UNIQUE KEY `id` (`id`),
  KEY `id_fil` (`id_fil`),
  KEY `id_adresse` (`id_adresse`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `enseignant`
--

INSERT INTO `enseignant` (`matricule_ensg`, `email`, `nom`, `PASSWORD`, `prenom`, `date_nais`, `id_fil`, `id_adresse`, `id`, `photo_profil`) VALUES
(':', '', '', '', NULL, NULL, NULL, NULL, 1, NULL),
('04-inf-15', 'prof@gmail.com', 'EKOBO', 'qwerty123', 'Brice', '2022-05-19', 'INF', 1, 2, '/media/photo_profil_ens/2.jpg'),
('ad-phy-01', 'l@gmail.com', 'zogo', 'qwerty123', 'franck', '2022-06-17', NULL, NULL, 4, '/media/photo_profil/default.jpg'),
('ens-phy-01', 'loraine@gmail.com', 'magne', 'qwerty123', 'vanessa', '2022-06-23', NULL, NULL, 3, '/media/photo_profil/default.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `etudiant`
--

DROP TABLE IF EXISTS `etudiant`;
CREATE TABLE IF NOT EXISTS `etudiant` (
  `matricule_etd` varchar(9) NOT NULL,
  `nom` varchar(25) DEFAULT NULL,
  `prenom` varchar(25) DEFAULT NULL,
  `date_nais` date DEFAULT NULL,
  `id_fil` varchar(10) DEFAULT NULL,
  `id_niveau` int DEFAULT NULL,
  `id_adresse` int DEFAULT NULL,
  `statut` enum('0','1') NOT NULL,
  `photo_profil` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '/media/photo_profil/default.jpg',
  `password` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `sexe` enum('M','F') NOT NULL,
  `pays` varchar(50) NOT NULL,
  `ville` varchar(50) NOT NULL,
  `savoir_faire` varchar(250) NOT NULL,
  `email` varchar(50) NOT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`matricule_etd`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `id` (`id`) USING BTREE,
  KEY `id_fil` (`id_fil`),
  KEY `id_niveau` (`id_niveau`),
  KEY `id_adresse` (`id_adresse`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `etudiant`
--

INSERT INTO `etudiant` (`matricule_etd`, `nom`, `prenom`, `date_nais`, `id_fil`, `id_niveau`, `id_adresse`, `statut`, `photo_profil`, `password`, `sexe`, `pays`, `ville`, `savoir_faire`, `email`, `id`) VALUES
('18T2713', 'Mbappe', 'Kylian', '2022-05-21', 'INF', 3, 2, '1', '/media/photo_profil/1.jpg', 'qwerty123', 'M', 'Rwanda', 'Kigali', 'developpeur full stack', 'mbappe@gmail.com', 1),
('19Y084', 'nzadouo', 'steave', '2003-03-14', 'INF', 3, NULL, '1', '/media/photo_profil/default.jpg', 'qwerty123', 'M', 'Cameroun', 'Yaounde', 'je frequente', 'steave@yahoo.com', 8),
('19Y085', 'nzadouo', 'franck', '2022-06-24', 'INF', 3, 0, '1', '/media/photo_profil/7.jpg', 'qwerty123', 'M', 'Cameroun', 'Yaounde ', 'dfg', 'franck.inconnu@yahoo.com', 7),
('19Y460', 'Tchikam', 'Selme', NULL, 'INF', 3, NULL, '1', '/media/photo_profil/default.jpg', 'qwerty123', '', '', '', '', '', 12);

-- --------------------------------------------------------

--
-- Structure de la table `faire_cours`
--

DROP TABLE IF EXISTS `faire_cours`;
CREATE TABLE IF NOT EXISTS `faire_cours` (
  `code` varchar(10) NOT NULL,
  `mat_ensg` varchar(10) NOT NULL,
  `id_fil` varchar(10) NOT NULL,
  `id_heure` int NOT NULL,
  `id_jour` int NOT NULL,
  `id_niveau` int NOT NULL,
  `id_salle` int NOT NULL,
  `id_semestre` int NOT NULL,
  `id_groupe` int NOT NULL,
  `id` int NOT NULL,
  PRIMARY KEY (`code`,`mat_ensg`,`id_fil`,`id_heure`,`id_jour`,`id_niveau`,`id_salle`,`id_semestre`,`id_groupe`,`id`),
  KEY `id_filier` (`id_fil`),
  KEY `id_h` (`id_heure`),
  KEY `id_j` (`id_jour`),
  KEY `id_nivea` (`id_niveau`),
  KEY `id_s` (`id_salle`),
  KEY `id_se` (`id_semestre`),
  KEY `mat-eng` (`mat_ensg`),
  KEY `id_groupe` (`id_groupe`),
  KEY `id_specialite` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `faire_cours`
--

INSERT INTO `faire_cours` (`code`, `mat_ensg`, `id_fil`, `id_heure`, `id_jour`, `id_niveau`, `id_salle`, `id_semestre`, `id_groupe`, `id`) VALUES
('info2026', '2022', 'biov', 1, 2, 2, 2, 2, 3, 3),
('info2026', '2022', 'chimie', 1, 1, 2, 1, 2, 2, 2),
('info3036', '18N2805', 'INF', 1, 2, 3, 3, 1, 0, 2),
('info3036', '1L805', 'INF', 1, 1, 4, 2, 2, 0, 2),
('info3036', '2022', 'INF', 1, 3, 4, 1, 2, 3, 2),
('info3037', '18S805', 'chimie', 1, 3, 2, 3, 2, 2, 2),
('info3037', '2022', 'chimie', 1, 6, 2, 3, 2, 2, 2),
('info3036', '1L8090', 'chimie', 2, 3, 2, 1, 2, 0, 2),
('info3037', '18S805', 'biov', 2, 3, 2, 2, 2, 2, 3),
('info3036', '2022', 'INF', 3, 1, 4, 1, 2, 0, 2),
('ikjd', '2022', 'chimie', 5, 5, 2, 1, 2, 3, 2);

-- --------------------------------------------------------

--
-- Structure de la table `fichier`
--

DROP TABLE IF EXISTS `fichier`;
CREATE TABLE IF NOT EXISTS `fichier` (
  `id_fichier` int NOT NULL AUTO_INCREMENT,
  `descriptif` varchar(25) NOT NULL,
  `contenu` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_fichier`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `fichier`
--

INSERT INTO `fichier` (`id_fichier`, `descriptif`, `contenu`) VALUES
(33, 'dddddd', '/media/cours/SSN MCD.pdf'),
(34, 'TD_1', '/media/cours/INFO306_TD 1.pdf'),
(57, 'TP', '/media/cours/MCD GESTION DES EMPLOIS DE TEMPS.pdf'),
(58, 'TD', '/media/cours/SSN MCD.pdf'),
(59, 'cours1', '/media/cours/19Y085_NZADOUO_DJANDJO_FRANCK_ARTHUR.'),
(60, '', '/media/publication/35.jpg'),
(61, 'essaie 23', '/media/publication/36.mp4'),
(62, '', '/media/publication/37.mp4');

-- --------------------------------------------------------

--
-- Structure de la table `filiere`
--

DROP TABLE IF EXISTS `filiere`;
CREATE TABLE IF NOT EXISTS `filiere` (
  `id_fil` varchar(10) NOT NULL,
  `nom_fil` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id_fil`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `filiere`
--

INSERT INTO `filiere` (`id_fil`, `nom_fil`) VALUES
('INF', 'informatique'),
('MATH', 'mathematiques'),
('PHY', 'physique');

-- --------------------------------------------------------

--
-- Structure de la table `filiere_niveau`
--

DROP TABLE IF EXISTS `filiere_niveau`;
CREATE TABLE IF NOT EXISTS `filiere_niveau` (
  `id_filiere` varchar(10) NOT NULL,
  `id_niveau` int NOT NULL,
  PRIMARY KEY (`id_filiere`,`id_niveau`),
  KEY `id_niv` (`id_niveau`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `filiere_niveau`
--

INSERT INTO `filiere_niveau` (`id_filiere`, `id_niveau`) VALUES
('biov', 1),
('chimie', 1),
('ICT', 1),
('phys', 1),
('scien', 1),
('biov', 2),
('chimie', 2),
('ICT', 2),
('phys', 2),
('scien', 2),
('biov', 3),
('chimie', 3),
('ICT', 3),
('phys', 3),
('scien', 3),
('biov', 4),
('chimie', 4),
('ICT', 4),
('phys', 4),
('scien', 4),
('biov', 5),
('chimie', 5),
('ICT', 5),
('phys', 5),
('scien', 5);

-- --------------------------------------------------------

--
-- Structure de la table `groupe`
--

DROP TABLE IF EXISTS `groupe`;
CREATE TABLE IF NOT EXISTS `groupe` (
  `id_groupe` int NOT NULL,
  `nom_groupe` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id_groupe`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `groupe`
--

INSERT INTO `groupe` (`id_groupe`, `nom_groupe`) VALUES
(0, 'Pas de groupe'),
(1, 'Groupe 1'),
(2, 'Groupe 2'),
(3, 'Groupe 3'),
(4, 'Groupe 4');

-- --------------------------------------------------------

--
-- Structure de la table `heure`
--

DROP TABLE IF EXISTS `heure`;
CREATE TABLE IF NOT EXISTS `heure` (
  `id_heure` int NOT NULL,
  `heure` varchar(25) NOT NULL,
  PRIMARY KEY (`id_heure`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `heure`
--

INSERT INTO `heure` (`id_heure`, `heure`) VALUES
(1, '07H00 - 09H55'),
(2, '10H05 - 12H55'),
(3, '13H15 - 15H55'),
(4, '16H05 - 18H55'),
(5, '19H05 - 21H55');

-- --------------------------------------------------------

--
-- Structure de la table `jour`
--

DROP TABLE IF EXISTS `jour`;
CREATE TABLE IF NOT EXISTS `jour` (
  `id_jour` int NOT NULL,
  `jour` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id_jour`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `jour`
--

INSERT INTO `jour` (`id_jour`, `jour`) VALUES
(1, 'Lundi'),
(2, 'Mardi'),
(3, 'Mercredi'),
(4, 'Jeudi'),
(5, 'Vendredi'),
(6, 'Samedi'),
(7, 'Dimanche');

-- --------------------------------------------------------

--
-- Structure de la table `lier`
--

DROP TABLE IF EXISTS `lier`;
CREATE TABLE IF NOT EXISTS `lier` (
  `code` varchar(10) NOT NULL,
  `id_fichier` int NOT NULL,
  `id_type` int NOT NULL,
  `date_publication` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`code`,`id_fichier`,`id_type`),
  KEY `id_fichier` (`id_fichier`),
  KEY `id_type` (`id_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `lier`
--

INSERT INTO `lier` (`code`, `id_fichier`, `id_type`, `date_publication`) VALUES
('INF3015', 34, 1, '2022-05-31 12:54:31'),
('INF3015', 59, 1, '2022-06-06 12:13:58'),
('INF3036', 33, 1, '2022-05-30 21:08:24'),
('INF3036', 57, 1, '2022-06-06 12:12:23'),
('INF3036', 61, 2, '2022-06-15 10:58:48'),
('INF3036', 62, 2, '2022-06-15 11:19:43'),
('INF3046', 58, 1, '2022-06-06 12:12:57'),
('INF3046', 60, 4, '2022-06-15 10:56:50');

-- --------------------------------------------------------

--
-- Structure de la table `liked`
--

DROP TABLE IF EXISTS `liked`;
CREATE TABLE IF NOT EXISTS `liked` (
  `id_publication` int DEFAULT NULL,
  `matricule_ensg` varchar(10) DEFAULT NULL,
  `matricule_etd` varchar(9) DEFAULT NULL,
  `status` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '1',
  `id` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `id_publication` (`id_publication`),
  KEY `matricule_ensg` (`matricule_ensg`),
  KEY `matricule_etd` (`matricule_etd`)
) ENGINE=MyISAM AUTO_INCREMENT=178 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `liked`
--

INSERT INTO `liked` (`id_publication`, `matricule_ensg`, `matricule_etd`, `status`, `id`) VALUES
(24, NULL, '19Y085', '1', 138),
(0, '0', '0', '1', 78),
(22, NULL, '19Y085', '1', 123),
(23, NULL, '19Y085', '1', 128),
(29, NULL, '19Y085', '1', 161),
(28, NULL, '19Y085', '1', 170),
(30, NULL, '19Y085', '1', 162),
(33, NULL, '04-inf-15', '1', 167),
(31, NULL, '19Y085', '1', 174),
(34, NULL, '19Y085', '1', 175),
(35, NULL, '19Y085', '1', 177);

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

DROP TABLE IF EXISTS `message`;
CREATE TABLE IF NOT EXISTS `message` (
  `id_message` int NOT NULL,
  `contenu` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `matricule_etd_envoi` varchar(9) DEFAULT NULL,
  `matricule_etd_desti` varchar(9) DEFAULT NULL,
  PRIMARY KEY (`id_message`),
  KEY `matricule_etd_envoi` (`matricule_etd_envoi`),
  KEY `matricule_etd_desti` (`matricule_etd_desti`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `niveau`
--

DROP TABLE IF EXISTS `niveau`;
CREATE TABLE IF NOT EXISTS `niveau` (
  `id_niveau` int NOT NULL AUTO_INCREMENT,
  `nom_niveau` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id_niveau`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `niveau`
--

INSERT INTO `niveau` (`id_niveau`, `nom_niveau`) VALUES
(1, 'niveau 1'),
(2, 'niveau 2'),
(3, 'niveau 3'),
(4, 'niveau 4'),
(5, 'niveau 5');

-- --------------------------------------------------------

--
-- Structure de la table `niveau_specialite`
--

DROP TABLE IF EXISTS `niveau_specialite`;
CREATE TABLE IF NOT EXISTS `niveau_specialite` (
  `id_niveau` int NOT NULL,
  `id_specialite` int NOT NULL,
  PRIMARY KEY (`id_niveau`,`id_specialite`),
  KEY `id_spe` (`id_specialite`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `note`
--

DROP TABLE IF EXISTS `note`;
CREATE TABLE IF NOT EXISTS `note` (
  `id_note` int NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  `matricule_etd` varchar(9) NOT NULL,
  `note_cc` float DEFAULT NULL,
  `note_tp` float DEFAULT NULL,
  `note_sn` float DEFAULT NULL,
  `id_sem` int DEFAULT NULL,
  PRIMARY KEY (`id_note`),
  KEY `code` (`code`),
  KEY `matricule_etd` (`matricule_etd`),
  KEY `id_sem` (`id_sem`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `note`
--

INSERT INTO `note` (`id_note`, `code`, `matricule_etd`, `note_cc`, `note_tp`, `note_sn`, `id_sem`) VALUES
(1, 'INF3036', '18T2713', 19, 20, 21, 2),
(2, 'INF3015', '19Y085', 20, 20, 20, 1),
(3, 'INF3036', '19Y085', 20, 20, 20, 1),
(4, 'INF3046', '19Y085', 20, 20, 20, 2);

-- --------------------------------------------------------

--
-- Structure de la table `publication`
--

DROP TABLE IF EXISTS `publication`;
CREATE TABLE IF NOT EXISTS `publication` (
  `id_publication` int NOT NULL AUTO_INCREMENT,
  `matricule_ensg` varchar(15) DEFAULT NULL,
  `matricule_etd` varchar(9) DEFAULT NULL,
  `titre` varchar(35) DEFAULT NULL,
  `date_pub` datetime DEFAULT CURRENT_TIMESTAMP,
  `description` varchar(200) DEFAULT NULL,
  `liked` int DEFAULT '0',
  `code` varchar(10) DEFAULT NULL,
  `id_fichier` int DEFAULT NULL,
  `id_fil` varchar(10) DEFAULT NULL,
  `id_niveau` int DEFAULT NULL,
  `status` enum('1','0','2') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_publication`),
  KEY `matricule_ensg` (`matricule_ensg`),
  KEY `id_fichier` (`id_fichier`),
  KEY `id_fil` (`id_fil`),
  KEY `id_niveau` (`id_niveau`),
  KEY `publication_ibfk_5` (`matricule_etd`),
  KEY `code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `publication`
--

INSERT INTO `publication` (`id_publication`, `matricule_ensg`, `matricule_etd`, `titre`, `date_pub`, `description`, `liked`, `code`, `id_fichier`, `id_fil`, `id_niveau`, `status`) VALUES
(28, NULL, '19Y085', 'essaie1', '2022-06-06 01:52:17', 'lkjh', 1, 'INF3046', NULL, 'INF', 3, '2'),
(31, '04-inf-15', NULL, 'essaie 1', '2022-06-06 04:08:06', 'kk', 1, 'INF3046', NULL, 'INF', 3, '1'),
(34, NULL, '19Y085', 'essaie 2', '2022-06-07 12:15:04', 'je veux voir si mon fichier sera accepte', 1, 'INF3046', NULL, 'INF', 3, '1'),
(35, NULL, '19Y085', 'essqie', '2022-06-15 10:56:51', '', 1, NULL, 60, 'INF', 3, '1'),
(36, NULL, '19Y085', 'titre1111111', '2022-06-15 10:58:48', 'essaie 23', 0, NULL, 61, 'INF', 3, '2'),
(37, NULL, '19Y085', 'essaie3', '2022-06-15 11:19:43', '', 0, NULL, 62, 'INF', 3, '0'),
(38, NULL, '19Y085', 'titr', '2022-06-15 12:06:21', 'essaie\r\n', 0, 'INF3046', NULL, 'INF', 3, '0');

-- --------------------------------------------------------

--
-- Structure de la table `salle`
--

DROP TABLE IF EXISTS `salle`;
CREATE TABLE IF NOT EXISTS `salle` (
  `id_salle` int NOT NULL,
  `nom_salle` varchar(25) NOT NULL,
  `capacité` varchar(25) NOT NULL,
  PRIMARY KEY (`id_salle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `salle`
--

INSERT INTO `salle` (`id_salle`, `nom_salle`, `capacité`) VALUES
(1, 's008', '15'),
(2, 's007', '15'),
(3, 's0012', '100'),
(4, 's006', '150'),
(5, 'AII', '250'),
(6, 'amphi502', '502');

-- --------------------------------------------------------

--
-- Structure de la table `semestre`
--

DROP TABLE IF EXISTS `semestre`;
CREATE TABLE IF NOT EXISTS `semestre` (
  `id_sem` int NOT NULL AUTO_INCREMENT,
  `nom_sem` varchar(55) NOT NULL,
  PRIMARY KEY (`id_sem`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `semestre`
--

INSERT INTO `semestre` (`id_sem`, `nom_sem`) VALUES
(1, 'Semestre 1'),
(2, 'Semestre 2');

-- --------------------------------------------------------

--
-- Structure de la table `specialite`
--

DROP TABLE IF EXISTS `specialite`;
CREATE TABLE IF NOT EXISTS `specialite` (
  `id` int NOT NULL,
  `specialite` varchar(25) DEFAULT NULL,
  `id_filiere` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `specialite`
--

INSERT INTO `specialite` (`id`, `specialite`, `id_filiere`) VALUES
(2, 'genie logiciel', 'INF'),
(3, 'reseau', 'INF'),
(4, 'sécurité', 'INF'),
(5, 'electronique', 'phys'),
(6, 'Tronc_commun', 'INF'),
(7, 'Tronc_commun', 'maths'),
(8, 'Tronc_commun', 'ICT'),
(9, 'Tronc_commun', 'chimie'),
(10, 'Tronc_commun', 'biov'),
(11, 'Tronc_commun', 'phys'),
(12, 'Tronc_commun', 'bios'),
(2, 'genie logiciel', 'INF'),
(3, 'reseau', 'INF'),
(4, 'sécurité', 'INF'),
(5, 'electronique', 'phys'),
(6, 'Tronc_commun', 'INF'),
(7, 'Tronc_commun', 'maths'),
(8, 'Tronc_commun', 'ICT'),
(9, 'Tronc_commun', 'chimie'),
(10, 'Tronc_commun', 'biov'),
(11, 'Tronc_commun', 'phys'),
(12, 'Tronc_commun', 'bios');

-- --------------------------------------------------------

--
-- Structure de la table `ssn_app_chatmessage`
--

DROP TABLE IF EXISTS `ssn_app_chatmessage`;
CREATE TABLE IF NOT EXISTS `ssn_app_chatmessage` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `message` longtext COLLATE utf8mb4_general_ci NOT NULL,
  `timestamp` datetime(6) NOT NULL,
  `thread_id` bigint DEFAULT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ssn_app_chatmessage_thread_id_8808ffc9` (`thread_id`),
  KEY `ssn_app_chatmessage_user_id_f8dc16bd` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ssn_app_cours_doc`
--

DROP TABLE IF EXISTS `ssn_app_cours_doc`;
CREATE TABLE IF NOT EXISTS `ssn_app_cours_doc` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `cours` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `ssn_app_cours_doc`
--

INSERT INTO `ssn_app_cours_doc` (`id`, `cours`) VALUES
(1, 'cours/SSN_MCD.pdf'),
(2, 'cours/INFO306_TD_1.pdf'),
(3, 'cours/MCD_GESTION_DES_EMPLOIS_DE_TEMPS.pdf'),
(4, 'cours/SSN_MCD_y8RB59S.pdf'),
(5, 'cours/19Y085_NZADOUO_DJANDJO_FRANCK_ARTHUR.pdf');

-- --------------------------------------------------------

--
-- Structure de la table `ssn_app_file_public`
--

DROP TABLE IF EXISTS `ssn_app_file_public`;
CREATE TABLE IF NOT EXISTS `ssn_app_file_public` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `publication` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `ssn_app_file_public`
--

INSERT INTO `ssn_app_file_public` (`id`, `publication`) VALUES
(1, 'publication/topo_bus.png'),
(2, 'publication/22.png'),
(3, 'publication/23.jpg'),
(4, 'publication/24.mp4'),
(5, 'publication/26.mp4'),
(6, 'publication/29.mp4'),
(7, 'publication/30.jpg'),
(8, 'publication/1647002024680.jpg'),
(9, 'publication/33.png'),
(10, 'publication/35.jpg'),
(11, 'publication/36.mp4'),
(12, 'publication/37.mp4');

-- --------------------------------------------------------

--
-- Structure de la table `ssn_app_image`
--

DROP TABLE IF EXISTS `ssn_app_image`;
CREATE TABLE IF NOT EXISTS `ssn_app_image` (
  `img` longblob NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `ssn_app_image`
--

INSERT INTO `ssn_app_image` (`img`) VALUES
(''),
(''),
(''),
(''),
(''),
(''),
(''),
(''),
(''),
(''),
(''),
(''),
(''),
(''),
(0x70686f746f5f70726f66696c2f746f706f5f6275732e706e67),
(0x70686f746f5f70726f66696c2f626c61636b5f726175672e6a7067),
(0x70686f746f5f70726f66696c2f494d472d32303232303430372d5741303033312e6a7067),
(0x70686f746f5f70726f66696c2f494d472d32303232303430372d5741303033315f474144736265472e6a7067),
(0x70686f746f5f70726f66696c2f494d472d32303232303430372d5741303032332e6a7067),
(0x70686f746f5f70726f66696c2f372e6a7067),
(0x70686f746f5f70726f66696c2f372e6a7067),
(0x70686f746f5f70726f66696c2f375f755930424f49762e6a7067),
(0x70686f746f5f70726f66696c2f375f647936764368382e6a7067),
(0x70686f746f5f70726f66696c2f375f6675346d5678482e6a7067),
(0x70686f746f5f70726f66696c2f375f6b4c6a574570472e6a7067),
(0x70686f746f5f70726f66696c2f372e6a7067),
(0x70686f746f5f70726f66696c2f372e6a7067),
(0x70686f746f5f70726f66696c2f375f6f6967556c63372e6a7067),
(0x70686f746f5f70726f66696c2f372e6a7067),
(0x70686f746f5f70726f66696c2f312e6a7067),
(0x70686f746f5f70726f66696c2f312e6a7067),
(0x70686f746f5f70726f66696c2f312e6a7067),
(0x70686f746f5f70726f66696c2f62642d73616c6c652e706e67),
(0x70686f746f5f70726f66696c2f372e6a7067);

-- --------------------------------------------------------

--
-- Structure de la table `ssn_app_image_ens`
--

DROP TABLE IF EXISTS `ssn_app_image_ens`;
CREATE TABLE IF NOT EXISTS `ssn_app_image_ens` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `img` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `ssn_app_image_ens`
--

INSERT INTO `ssn_app_image_ens` (`id`, `img`) VALUES
(1, 'photo_profil_ens/2.jpg'),
(2, 'photo_profil_ens/2_jjKGFrp.jpg'),
(3, 'photo_profil_ens/2_Mb9ztyG.jpg'),
(4, 'photo_profil_ens/2.jpg'),
(5, 'photo_profil_ens/2_z2cW8Hi.jpg'),
(6, 'photo_profil_ens/2_Ril2IpZ.jpg'),
(7, 'photo_profil_ens/2_2mf39SW.jpg'),
(8, 'photo_profil_ens/2_XhK2XUQ.jpg'),
(9, 'photo_profil_ens/2_P4JUNY2.jpg'),
(10, 'photo_profil_ens/2.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `ssn_app_thread`
--

DROP TABLE IF EXISTS `ssn_app_thread`;
CREATE TABLE IF NOT EXISTS `ssn_app_thread` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `updated` datetime(6) NOT NULL,
  `timestamp` datetime(6) NOT NULL,
  `first_person_id` int DEFAULT NULL,
  `second_person_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ssn_app_thread_first_person_id_second_person_id_61ba340d_uniq` (`first_person_id`,`second_person_id`),
  KEY `ssn_app_thread_first_person_id_7e52de56` (`first_person_id`),
  KEY `ssn_app_thread_second_person_id_056182d6` (`second_person_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `temps`
--

DROP TABLE IF EXISTS `temps`;
CREATE TABLE IF NOT EXISTS `temps` (
  `n` int NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `type`
--

DROP TABLE IF EXISTS `type`;
CREATE TABLE IF NOT EXISTS `type` (
  `id_type` int NOT NULL,
  `type` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `type`
--

INSERT INTO `type` (`id_type`, `type`) VALUES
(1, 'MAGISTRAL'),
(2, 'PRATIQUE'),
(1, 'MAGISTRAL'),
(2, 'PRATIQUE');

-- --------------------------------------------------------

--
-- Structure de la table `type_fichier`
--

DROP TABLE IF EXISTS `type_fichier`;
CREATE TABLE IF NOT EXISTS `type_fichier` (
  `id_type` int NOT NULL AUTO_INCREMENT,
  `nom_type` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id_type`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `type_fichier`
--

INSERT INTO `type_fichier` (`id_type`, `nom_type`) VALUES
(1, 'texte'),
(2, 'video'),
(3, 'audio'),
(4, 'image');

-- --------------------------------------------------------

--
-- Structure de la table `ue_fil_sem`
--

DROP TABLE IF EXISTS `ue_fil_sem`;
CREATE TABLE IF NOT EXISTS `ue_fil_sem` (
  `id_ue` varchar(25) NOT NULL,
  `id_niveau` int NOT NULL,
  `id_filiere` varchar(10) NOT NULL,
  `id_semestre` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `ue_fil_sem`
--

INSERT INTO `ue_fil_sem` (`id_ue`, `id_niveau`, `id_filiere`, `id_semestre`) VALUES
('info2026', 2, 'INF', 2),
('info3036', 3, 'INF', 2),
('info3037', 3, 'INF', 1),
('info3225', 3, 'mios', 1),
('info3226', 3, 'chimie', 1),
('maths1101', 1, 'maths', 1),
('PPE', 2, 'INF', 2),
('info2026', 2, 'INF', 2),
('info3036', 3, 'INF', 2),
('info3037', 3, 'INF', 1),
('info3225', 3, 'mios', 1),
('info3226', 3, 'chimie', 1),
('maths1101', 1, 'maths', 1),
('PPE', 2, 'INF', 2);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `acceder`
--
ALTER TABLE `acceder`
  ADD CONSTRAINT `acceder_ibfk_1` FOREIGN KEY (`code`) REFERENCES `cours` (`code`),
  ADD CONSTRAINT `acceder_ibfk_2` FOREIGN KEY (`matricule_etd`) REFERENCES `etudiant` (`matricule_etd`);

--
-- Contraintes pour la table `commentaire`
--
ALTER TABLE `commentaire`
  ADD CONSTRAINT `commentaire_ibfk_1` FOREIGN KEY (`id_publication`) REFERENCES `publication` (`id_publication`),
  ADD CONSTRAINT `commentaire_ibfk_2` FOREIGN KEY (`matricule_ensg`) REFERENCES `enseignant` (`matricule_ensg`),
  ADD CONSTRAINT `commentaire_ibfk_3` FOREIGN KEY (`matricule_etd`) REFERENCES `etudiant` (`matricule_etd`);

--
-- Contraintes pour la table `cours`
--
ALTER TABLE `cours`
  ADD CONSTRAINT `cours_ibfk_1` FOREIGN KEY (`id_fil`) REFERENCES `filiere` (`id_fil`),
  ADD CONSTRAINT `cours_ibfk_2` FOREIGN KEY (`id_niveau`) REFERENCES `niveau` (`id_niveau`),
  ADD CONSTRAINT `cours_ibfk_3` FOREIGN KEY (`matricule_ensg`) REFERENCES `enseignant` (`matricule_ensg`);

--
-- Contraintes pour la table `enseignant`
--
ALTER TABLE `enseignant`
  ADD CONSTRAINT `enseignant_ibfk_1` FOREIGN KEY (`id_fil`) REFERENCES `filiere` (`id_fil`),
  ADD CONSTRAINT `enseignant_ibfk_2` FOREIGN KEY (`id_adresse`) REFERENCES `adresse` (`id_adresse`);

--
-- Contraintes pour la table `etudiant`
--
ALTER TABLE `etudiant`
  ADD CONSTRAINT `etudiant_ibfk_1` FOREIGN KEY (`id_fil`) REFERENCES `filiere` (`id_fil`),
  ADD CONSTRAINT `etudiant_ibfk_2` FOREIGN KEY (`id_niveau`) REFERENCES `niveau` (`id_niveau`),
  ADD CONSTRAINT `etudiant_ibfk_3` FOREIGN KEY (`id_adresse`) REFERENCES `adresse` (`id_adresse`);

--
-- Contraintes pour la table `lier`
--
ALTER TABLE `lier`
  ADD CONSTRAINT `lier_ibfk_1` FOREIGN KEY (`id_fichier`) REFERENCES `fichier` (`id_fichier`),
  ADD CONSTRAINT `lier_ibfk_2` FOREIGN KEY (`id_type`) REFERENCES `type_fichier` (`id_type`),
  ADD CONSTRAINT `lier_ibfk_3` FOREIGN KEY (`code`) REFERENCES `cours` (`code`);

--
-- Contraintes pour la table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `message_ibfk_1` FOREIGN KEY (`matricule_etd_envoi`) REFERENCES `etudiant` (`matricule_etd`),
  ADD CONSTRAINT `message_ibfk_2` FOREIGN KEY (`matricule_etd_desti`) REFERENCES `etudiant` (`matricule_etd`);

--
-- Contraintes pour la table `note`
--
ALTER TABLE `note`
  ADD CONSTRAINT `note_ibfk_1` FOREIGN KEY (`code`) REFERENCES `cours` (`code`),
  ADD CONSTRAINT `note_ibfk_2` FOREIGN KEY (`matricule_etd`) REFERENCES `etudiant` (`matricule_etd`),
  ADD CONSTRAINT `note_ibfk_3` FOREIGN KEY (`id_sem`) REFERENCES `semestre` (`id_sem`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `publication`
--
ALTER TABLE `publication`
  ADD CONSTRAINT `publication_ibfk_1` FOREIGN KEY (`matricule_ensg`) REFERENCES `enseignant` (`matricule_ensg`),
  ADD CONSTRAINT `publication_ibfk_2` FOREIGN KEY (`id_fichier`) REFERENCES `fichier` (`id_fichier`),
  ADD CONSTRAINT `publication_ibfk_3` FOREIGN KEY (`id_fil`) REFERENCES `filiere` (`id_fil`),
  ADD CONSTRAINT `publication_ibfk_4` FOREIGN KEY (`id_niveau`) REFERENCES `niveau` (`id_niveau`),
  ADD CONSTRAINT `publication_ibfk_5` FOREIGN KEY (`matricule_etd`) REFERENCES `etudiant` (`matricule_etd`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `publication_ibfk_6` FOREIGN KEY (`code`) REFERENCES `cours` (`code`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
